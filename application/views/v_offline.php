<!DOCTYPE html>
<!-- 
***************************************************************
* PROJECT : NCHype Under-Construction Template
* AUTHOR : NCodeArt
***************************************************************
-->
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="description" content="">
	<meta name="keywords" content="nc,ncodeart,slideshow,animation,youtube background,coming-soon,underconstruction">
	<meta name="author" content="NCodeArt">
	
	<title>NEXBAY OFFLINE PRODUCT</title>

	<!-- GOOGLE FONTS -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

	<!-- FONT-ICON -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/offline/lib/et-line-font/style.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/offline/lib/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/offline/lib/Icon-font-7-stroke-PIXEDEN/css/pe-icon-7-stroke.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/offline/lib/themify-icons/themify-icons.css">

	<!-- DEFAULT-LIB -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/offline/lib/animation/animate.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/offline/lib/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/offline/lib/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.min.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/offline/lib/Magnific-Popup/magnific-popup.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/offline/lib/vegas/vegas.min.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/offline/lib/star-animation/star-animation.css">

	<!-- TEMPLATE -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/offline/lib/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/offline/css/nc-structure.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/offline/css/nc-main.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/offline/css/nc-helper.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/offline/css/nc-responsive.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/offline/css/custom.css">

</head>
<body>
	
	<!-- PAGE-LOADER -->
	<div class="page-loader-wrapper page-loader-wrapper-3 flex-cc">
		<div class="loader">
			<div class="spinner">
				<div class="circle1"></div>
				<div class="circle2"></div>
			</div>
		</div>
	</div><!-- / PAGE-LOADER -->

	<!-- MAIN-WRAPPER -->
	<div id="main-wrapper" class="main-wrapper">
		
		<!-- BACKGROUND -->
		<div class="bg-section bg-cover bg-cc" data-bgImage="<?=base_url()?>assets/offline/images/bg-19.jpg">
			<div class="overlay" style="background-color: rgba(0,0,0,0.35);"></div>

			<!-- STAR ANIMATION -->
			<div class="bg-animation">
				<div id='stars'></div>
				<div id='stars2'></div>
				<div id='stars3'></div>
				<div id='stars4'></div>
			</div><!-- / STAR ANIMATION -->
			
		</div><!-- / BACKGROUND -->

		<!-- HOME-WRAPPER -->
		<div id="home-wrapper" class="flex-cc page-wrapper">
			<div class="inner-wrapper">
				
				<!-- HOME-PAGE -->
				<div class="intro-section intro-section-1 animated s0-8" data-animOut="fadeOut" data-animIn="fadeIn">
					<div class="container">
						
						<!-- LOGO -->
						<div class="logo">
							<img src="<?=base_url()?>assets/geniot/images/logo.png" alt="NCodeArt">
						</div><!-- / LOGO -->

						<!-- TEXT-ANIMATION -->
						<div class="text-animation">
							<div class="hd-text">
								<h2 class="main-text">NEXBAY.ID</h2>
							</div>
							<div class="carousel-widget" data-anim="fadeInUp" id="carousel-widget" data-items="1" data-itemrange="false" data-tdrag="false" data-mdrag="false" data-pdrag="false" data-autoplay="true" data-in="fadeIn" data-loop="true" data-hstop="true">
								<div class="owl-carousel">
									<div class="item"><b style="font-size:20px;font-weight:200" class="main-text">PRODUK HABIS TERJUAL. LIHAT PRODUK LAINNYA DI <a style="font-weight:bold" href="http://nexbay.id">NEXBAY.ID</a></b></div>
								</div>
							</div>
						</div><!-- / TEXT-ANIMATION -->
						<br><br>
						<!-- NAVIGATION -->
						<div class="navigation-wrp navigation-wrp-1">
							<div class="inner-wrp">
								<a href="http://nexbay.id" class="nav-link hvr-sweep-to-right">KEMBALI KE HALAMAN UTAMA</a>
							</div>
						</div>

					</div>
				</div>
				<!-- / HOME-PAGE -->

			</div>

		</div><!-- / HOME-WRAPPER -->

	</div><!-- / MAIN-WRAPPER -->

	<!-- DEFAULT-SCRIPT -->
	<script src="<?=base_url()?>assets/offline/lib/jquery/jquery-1.11.3.min.js"></script>
	<script src="<?=base_url()?>assets/offline/lib/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/offline/lib/jquery-validation/jquery.validate.min.js"></script>

	<!-- PLUGIN SCRIPT -->
	<script src="<?=base_url()?>assets/offline/js/plugins.js"></script>

	<!-- TEMPLATE SCRIPT -->
	<script src="<?=base_url()?>assets/offline/js/nc-common.js"></script>
</body>
</html>