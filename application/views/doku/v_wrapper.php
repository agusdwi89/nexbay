<!DOCTYPE html>
<html lang="en">
	<head>
		<?=$this->load->view('fe/header');?>	
		<script type="text/javascript">
			var clog;
		</script>
		<script src="<?=base_url()?>assets/fe/js/jquery.min.js"></script>
	</head>

<body>
	<?=form_open('/',array('id'=>'global-form'))?>
	<?=form_close()?>

	<!-- Start Preloader -->
	<div id="page-preloader">
		<svg class="circular" height="50" width="50">
			<circle class="path" cx="25" cy="25" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"></circle>
		</svg>
	</div>
	<!-- End Preloader -->

	<!-- Start Content -->
	<section id="main" class="wrapper">

		<?
			$d['data'] 		= $dbr;
			$this->load->view("doku/$child",$d);
		?>

	</section>
	<!-- End Content -->
	<script type="text/javascript">
	$(function(){
		$(window).on('load', function () {
			$('#page-preloader').delay(1000).fadeOut('slow');

			$('.title-text').addClass('animation-right');
			$('.title-product').addClass('animation-left');
		});	
	})
	</script>

</html>