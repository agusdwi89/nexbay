<script type="text/javascript">
	$(function(){
		$('[data-plugin="switchery"]').each(function (idx, obj) {
			new Switchery($(this)[0], $(this).data());
		});
	})
</script>
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="page-title-box">
					<h4 class="page-title">Manage Top Menu</h4>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>

		<?php if ($this->sub_domain == "all"): ?>

			<div class="col-md-12">
				<div class="card">
					<center>
						<br><br>
						<h3>please select sub domain above</h3>
						<br><br>
					</center>
				</div>
			</div>

		<?php else:?>
			<div class="row">
				<div class="col-sm-7">
					<div class="card">
						<div class="card-body table-responsive">
							<h4 class="m-t-0 header-title">
								<b>List Top Menu</b>
								<a href="<?=base_url('manage/topmenu/add')?>" style="float: right" class="btn btn-info waves-effect waves-light btn-sm"> <i class="fa fa-plus m-r-5"></i> <span>Add Menu</span> </a>
							</h4>
							<br>

							<table class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
								<thead>
									<tr>
										<th>No</th>
										<th>Name</th>
										<th>Open</th>
										<th>URL</th>
										<!-- <th>Button Style</th> -->
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=0;foreach ($db->result() as $d): $i++;?>
										<tr>
											<td><?=$i;?></td>
											<td><?=$d->name;?></td>
											<td><?=$d->link;?></td>
											<td><?=$d->url;?></td>
											<!-- <td><?=$d->button;?></td> -->
											<td>
												<center>
													<a style="opacity:100 !important;margin-right:12px;float:initial;" title="edit" href="<?=base_url()?>manage/topmenu/edit/<?=$d->id;?>" class="fa fa-pencil delete-list dz-tip"></a>
													<a style="opacity:100 !important;margin-right:12px;float:initial;" title="delete" href="<?=base_url()?>manage/topmenu/delete/<?=$d->id;?>" class="fa fa-times-circle delete-list dz-tip confirm-delete"></a>
												</center>
											</td>
										</tr>
									<?php endforeach ?>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="card">
						<div class="card-body table-responsive">
							<h4 class="m-t-0 header-title">
								<b>Buy Button Config</b>
							</h4>
							<br>				
							<?=form_open('',array("class"=>"form-horizontal"))?>
	                            <div class="form-group row">
	                                <label class="col-md-12 control-label">Button Text</label>
	                                <div class="col-md-12">
	                                    <input type="text" placeholder="title text" class="form-control" name="value3" value="<?=$topbuy->value3;?>">
	                                </div>
	                            </div>
	                            <div class="form-group row">
	                                <label class="col-md-12 control-label">Button Link Section</label>
	                                <div class="col-md-12">
	                                	<select name="value" class=" form-control" title="">
	                                		<?php foreach ($section->result() as $s): ?>
	                                			<option <?=(($s->section_name == $topbuy->value)?"selected='selected'":"");?>><?=$s->section_name;?></option>
	                                		<?php endforeach ?>
	                                	</select>	                             
	                                </div>
	                            </div>
	                            <div class="form-group row">
	                                <label class="col-md-12 control-label">Active</label>
	                                <div class="col-md-12">
	                                    <input name="value2" data-id="1" class="checkcolor" type="checkbox" data-plugin="switchery" data-color="#ff5d48"  <?=(($topbuy->value2 == "on")?"checked":"");?>/>
	                                </div>
	                            </div>
	                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
	                        <?=form_close()?>
						</div>
					</div>
				</div>
			</div>
		<?php endif;?>
	</div>
</div>