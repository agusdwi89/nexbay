<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage About Section</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Edit About Section</h4>
                        <br>
                        <?=form_open_multipart('',array("class"=>"form-horizontal"))?>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Title</label>
                                <div class="col-md-3">
                                    <input type="text" placeholder="title text" class="form-control" name="ar[title_first]" value="<?=$items->title_first;?>">
                                </div>
                                <div class="col-md-3">
                                    <input type="text" placeholder="title text" class="form-control" name="ar[title_highlight]" value="<?=$items->title_highlight;?>">
                                </div>
                                <div class="col-md-3">
                                    <input type="text" placeholder="title text" class="form-control" name="ar[title_last]" value="<?=$items->title_last;?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Number</label>
                                <div class="col-md-2">
                                    <input type="text" placeholder="number" class="form-control" name="ar[number]" value="<?=$items->number;?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Description</label>
                                <div class="col-md-10">
                                    <textarea placeholder="Description text" class="form-control" rows="5" name="ar[description]"><?=$items->description;?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Video URL</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="youtube video id" class="form-control" name="ar[link]" value="<?=$items->link;?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Button Text</label>
                                <div class="col-md-2">
                                    <input type="text" name="ar[button_text]" class="form-control" value="<?=$items->button_text;?>" placeholder="Button text">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Button Type</label>
                                <div class="col-md-2">
                                    <select id="select-type" name="ar[button_type]" class=" form-control" title="">
                                        <option <?=(("section" == $items->button_type)?"selected='selected'":"");?> value="section">Section</option>
                                        <option <?=(("external" == $items->button_type)?"selected='selected'":"");?> value="external">External</option>
                                    </select>                                
                                </div>
                            </div>
                            <div id="state-section" class="form-group row state-option">
                                <label class="col-md-2 control-label">Section Link</label>
                                <div class="col-md-2">
                                    <select name="state[section]" class=" form-control" title="">
                                        <?php foreach ($sectionopt->result() as $s): ?>
                                            <option <?=(($s->section_name == $items->button_link)?"selected='selected'":"");?>><?=$s->section_name;?></option>
                                        <?php endforeach ?>
                                    </select>                                
                                </div>
                            </div>
                            <div id="state-external" class="form-group row state-option">
                                <label class="col-md-2 control-label">External URL</label>
                                <div class="col-md-2">
                                    <input type="text" name="state[external]" class="form-control" value="<?=$items->button_link;?>" placeholder="Button text">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">image</label>
                                <div class="col-md-10">
                                    <?if ($items->image_bg != ""): ?>
                                        <a target="_blank" href="<?=base_url()?>assets/section/<?=$items->image_bg;?>">
                                            <img class="image-section-header-placeholder" src="<?=base_url()?>assets/section/<?=$items->image_bg;?>">
                                        </a>
                                        <span>change image : </span>
                                    <?endif;?>
                                    <input type="file" class="default" name="image_bg">
                                    <br>
                                    <p class="text-muted m-b-25">* Image size 2000 x 1080 px , JPG & PNG allowed.</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">image play</label>
                                <div class="col-md-10">
                                    <?if ($items->image_bg_play != ""): ?>
                                        <a target="_blank" href="<?=base_url()?>assets/section/<?=$items->image_bg_play;?>">
                                            <img class="image-section-header-placeholder" src="<?=base_url()?>assets/section/<?=$items->image_bg_play;?>">
                                        </a>
                                        <span>change image : </span>
                                    <?endif;?>
                                    <input type="file" class="default" name="image_bg_play">
                                    <br>
                                    <p class="text-muted m-b-25">* Image size 170 x 170 px , JPG & PNG allowed.</p>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <button id="btn-backto-section" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Back to Section</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function(){
        $(".state-option").hide();
        $( "#select-type" ).change(function(e) {
           if ($(this).val() == "section") {
                $("#state-external").hide();
                $("#state-section").show();
            }else{
                $("#state-section").hide();
                $("#state-external").show();
            }
        });
        $( "#select-type" ).trigger('change');
    })
</script>