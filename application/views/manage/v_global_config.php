<? $tb = $this->tab ;?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Site Configuration</h4>
                    <div class="clearfix">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link <?=($tb == "social")? 'active' : '' ?>" id="social-tab" data-toggle="tab" href="#social" role="tab" aria-controls="social" aria-selected="true">
                                    <span class="d-block d-sm-none"><i class="fa fa-home"></i></span>
                                    <span class="d-none d-sm-block">Social Link</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?=($tb == "logo")? 'active' : '' ?>" id="logo-tab" data-toggle="tab" href="#logo" role="tab" aria-controls="logo" aria-selected="false">        
                                    <span class="d-block d-sm-none"><i class="fa fa-user"></i></span>
                                    <span class="d-none d-sm-block">Logo</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?=($tb == "pure")? 'active' : '' ?>" id="pure-tab" data-toggle="tab" href="#pure" role="tab" aria-controls="pure" aria-selected="false">
                                    <span class="d-block d-sm-none"><i class="fa fa-envelope-o"></i></span>
                                    <span class="d-none d-sm-block">Purechat</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?=($tb == "other_prod")? 'active' : '' ?>" id="other_prod-tab" data-toggle="tab" href="#other_prod" role="tab" aria-controls="other_prod" aria-selected="false">
                                    <span class="d-block d-sm-none"><i class="fa fa-envelope-o"></i></span>
                                    <span class="d-none d-sm-block">Other Product</span>
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane <?=($tb == "social")? 'show active' : '' ?>" id="social" role="tabpanel" aria-labelledby="social-tab">
                                <?=form_open()?>
                                    <?foreach ($data as $key => $value): ?>
                                        <div class="form-group row">
                                            <label class="col-md-3 control-label"><?=$key;?></label>
                                            <div class="col-md-9">
                                                <?php if ($key == "google-track" || $key == "fb-track"): ?>
                                                    <textarea style="font-size:10px" class="form-control" name="<?=$key;?>"><?=$value;?></textarea>
                                                <?php else: ?>
                                                    <input type="text" placeholder="<?=$key;?>" class="form-control" name="<?=$key;?>" value="<?=$value;?>">
                                                <?php endif ?>
                                            </div>
                                        </div>
                                    <?endforeach;?>
                                    <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                                <?=form_close()?>      
                            </div>

                            <div class="tab-pane <?=($tb == "logo")? 'show active' : '' ?>" id="logo" role="tabpanel" aria-labelledby="logo-tab">
                                <small>*If logo didnt change after upload, try clear cache or check live website on incognito mode, image need refresh from browser cache</small>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <?=form_open_multipart(base_url('manage/global_config/set_logo'))?>
                                            <div class="form-group row">
                                                <label class="col-md-3 control-label">Primary Logo</label>
                                                <div class="col-md-9">
                                                    <a target="_blank" href="<?=base_url()?>assets/geniot/images/logo.png">
                                                        <img style="width:166px !important" class="image-section-header-placeholder" src="<?=base_url()?>assets/geniot/images/logo.png">
                                                    </a>
                                                    <br><br>
                                                    <span>change image : </span>
                                                    <input type="file" class="default" name="userfile">
                                                    <br>
                                                    <p class="text-muted m-b-25">* Image size 166 x 100 px , PNG allowed.</p>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                                        <?=form_close()?>         
                                    </div>
                                    <div class="col-md-4">
                                        <?=form_open_multipart(base_url('manage/global_config/set_icon'))?>
                                            <div class="form-group row">
                                                <label class="col-md-3 control-label">Icon</label>
                                                <div class="col-md-9">
                                                    <a target="_blank" href="<?=base_url()?>assets/geniot/images/favicon.ico">
                                                        <img style="width:50px !important" class="image-section-header-placeholder" src="<?=base_url()?>assets/geniot/images/favicon.ico">
                                                    </a>
                                                    <br><br>
                                                    <span>change image : </span>
                                                    <input type="file" class="default" name="userfile">
                                                    <br>
                                                    <p class="text-muted m-b-25">* Image size 100 x 100 px , PNG allowed.</p>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                                        <?=form_close()?>         
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane <?=($tb == "pure")? 'show active' : '' ?>" id="pure" role="tabpanel" aria-labelledby="pure-tab">
                                <?=form_open(base_url('manage/global_config/set_pure'))?>
                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Pure Chat Code</label>
                                    <div class="col-md-9">
                                        <textarea style="font-size:10px" class="form-control" name="value"><?=$value_pure;?></textarea>
                                        <input type="hidden" name="name" value="pure-chat">
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                                <?=form_close()?>      
                            </div>

                            <div class="tab-pane <?=($tb == "other_prod")? 'show active' : '' ?>" id="other_prod" role="tabpanel" aria-labelledby="other_prod-tab">
                                <div class="row">
                                    
                                    <div class="col-md-6">
                                        <?=form_open(base_url('manage/global_config/set_other_prod'))?>
                                            <div class="form-group row">
                                                <label class="col-md-12 control-label">Name Product</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="name" class="form-control" placeholder="Button text">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-12 control-label">Url Product</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="url" class="form-control" placeholder="Button text">
                                                </div>
                                            </div>
                                            
                                            <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                                        <?=form_close()?>              
                                    </div>
                                    <div class="col-md-6">
                                        <table class="table table-space m-0">   
                                            <tr>
                                                <th>NO</th>
                                                <th>Name</th>
                                                <th>URL</th>
                                            </tr>
                                            <?$i=0;foreach ($other_prod->result() as $k):$i++;?>
                                            <tr>
                                                <td>
                                                    <?=$i;?>
                                                </td>
                                                <td>
                                                    <?=$k->name;?>
                                                </td>
                                                <td>
                                                    <a href="<?=$k->url;?>" target="_blank">
                                                        <?=$k->url;?>
                                                    </a>
                                                </td>
                                                <td>
                                                    <center>
                                                        <a style="opacity:100 !important;margin-right:12px" title="delete" href="<?=base_url()?>manage/global_config/delete_other/<?=$k->id;?>" class="fa fa-times-circle delete-list dz-tip confirm-delete"></a>
                                                    </center>
                                                </td>
                                            </tr>
                                            <?endforeach;?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>