<style type="text/css">
	#datatable_wrapper{opacity: 0}
	#datatable{opacity: 0}
	p.txwrap{
		max-width:200px !important;
		overflow-wrap: break-word !important;
		word-wrap: break-word !important;
		white-space: pre-wrap;
		line-height: 15px;
		font-size: 11px;
		font-style: italic;
	}
	a.btn-confirm-download span{
		font-size: 10px;
		display: block;
		float: right;
		margin-left: 5px;
	}
	a.btn-confirm-download i{
		margin-top: 9px;
	}
	#filter-order{
		padding: 20px;
		margin-bottom: 20px;
		border-radius: 10px;
		border: solid 1px black;
		width: 745px;margin: 0 auto 20px;
	}
	#filter-order .fa-caret-down{
		float: right;
		margin-right: 0px;
		margin-top: 2px;
		font-size: 15px;
	}
</style>
<script type="text/javascript">
	var start 			= moment().startOf('month');
	var end 			= moment().endOf('month');
	var url_batch 		= "<?=base_url('manage/order/batch')?>";
	var url_resi_batch 	= "<?=base_url('manage/order/batch_resi')?>";
	var ad = start.format('YYYY-MM-DD');var bd = end.format('YYYY-MM-DD');
	var GLOB_S;var GLOB_E;
	var GLOB_PAYMENT 	= "all";
	var GLOB_STATUS 	= "all";

	$(function(){
		function cb(start, end) {
			$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		}

		$('#reportrange').daterangepicker({
			startDate: moment().startOf('month'),
			endDate: moment().endOf('month'),
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			}
		}, cb);

		$('body').on('click','.download-csv-class',function (e) {
			var download_url = $(this).data('url');
			$(this).attr('href',download_url+'/'+GLOB_S+'s'+GLOB_E);
		});	


		cb(start, end);

		$('#reportrange').on('apply.daterangepicker', function(ev, picker) {
			ad = picker.startDate.format('YYYY-MM-DD');
			bd = picker.endDate.format('YYYY-MM-DD');
			load_order(ad,bd);
		});
		load_order(start.format('YYYY-MM-DD'),end.format('YYYY-MM-DD'));

		// batch
		$('.bs-example-modal-lg').on('show.bs.modal', function() { 
			$("#batch-modal-body2").html('');
			opt[$("#global-form input").attr('name')] = $("#global-form input").val();
			$.post( url_batch,opt, function( data ) {
				$("#batch-modal-body2").html(data);
			});
		}) ;

		// batch_resi
		$('.bs-modal-resi').on('show.bs.modal', function() { 
			$("#batch-modal-body-resi").html("");
			var opt = {s:ad,e:bd,p:GLOB_PAYMENT};
			opt[$("#global-form input").attr('name')] = $("#global-form input").val();
			$.post( url_resi_batch,opt, function( data ) {
				$("#batch-modal-body-resi").html(data);
			});
		}) ;

		$('body').on('click','#mdl-submit-cancel-refund',function (e) {
			e.preventDefault();
			$("#mdl-form-cancel-refund").fadeOut(function(){
				$("#mdl-form-loading").show();
				$("#mdl-form-loading h3").html('Loading . . .');
				$.post($("#mdl-form-cancel-refund").attr('action'), $("#mdl-form-cancel-refund").serialize(),function(){
					$("#mdl-form-loading h3").html('Data saved.');
					setTimeout(function () {
						$('.bs-modal-cancel-refund').modal('hide');
						setTimeout(function () {
							$("#mdl-form-cancel-refund").show();
							$("#mdl-form-loading").hide();
							load_order(GLOB_S,GLOB_E);
						},500)
					}, 1000);
				});	
			})
			
		});

		$( ".select-filter" ).change(function () {
			GLOB_PAYMENT = $('#select-payment-method').val();
			// GLOB_STATUS = $('#select-status-order').val();
			load_order(GLOB_S,GLOB_E);
		});

		$("#select-status-order").change(function(){
			var stat = $(this).val();
			$("#datatable_filter input").val(stat).keyup();
		})
	})

	function load_order(s,e){
		GLOB_S = s;GLOB_E = e;
		var url = "<?=base_url()?>manage/order/load/"+s+"s"+e+"/"+GLOB_PAYMENT+"/"+GLOB_STATUS;
		$("#main-load").html("");
		$("#loader-bar").show();
		$.get( url, function( data ) {
			$("#main-load").html(data);
		});
		$("#select-status-order").val('');
	}
</script>

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="page-title-box">
					<h4 class="page-title">Manage Order</h4>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-body table-responsive">
						<div class="row">
							<div class="col-md-12">
								<h4 style="line-height:30px" class="m-t-0 header-title pull-left">List Customer Order [ subdomain : <?=(($this->sub_domain == "all")? "All sub-domain" : $this->sub_domain.".".$_SERVER['HTTP_HOST']);?> ]</h4>
								<a data-url="<?=base_url('manage/order/download')?>" id="download-csv" title="download csv" href="#" style="float: right;margin-right:5px" class="download-csv-class btn btn-info waves-effect waves-light btn-sm dz-tip"><i class="fa fa-cloud-download"></i></a>
								<a data-url="<?=base_url('manage/order/download_cod')?>" id="download-csv-cod" title="download csv cod" href="#" style="float: right;margin-right:5px" class="download-csv-class btn btn-success waves-effect waves-light btn-sm dz-tip"><i class="fa fa-truck"></i></a>
								<a data-url="<?=base_url('manage/order/download_non_cod')?>" id="download-csv-non-cod" title="download csv non cod" href="#" style="float: right;margin-right:5px" class="download-csv-class btn btn-success waves-effect waves-light btn-sm dz-tip"><i class="fa fa-credit-card"></i></a>
								<a title="batch update status order" href="#" style="float: right;margin-right:5px" class="btn btn-warning waves-effect waves-light btn-sm dz-tip" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-pencil"></i></a>
								<a title="batch update resi" href="#" style="float: right;margin-right:5px" class="btn btn-warning waves-effect waves-light btn-sm dz-tip" data-toggle="modal" data-target=".bs-modal-resi"><i class="fa fa-sticky-note"></i></a>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-1"></div>
							<div id="filter-order">
								<div style="width:225px;float:left;margin-left:20px;">
									<b>Order Date :</b><br>
									<div id="reportrange" style="background: #fff;cursor: pointer;padding: 8px 10px;border: 1px solid #ccc;border-radius: 6px;">
										<i class="fa fa-calendar"></i>&nbsp;
										<span></span> <i class="fa fa-caret-down"></i>
									</div>	
								</div>
								<div style="width:200px;float:left;margin-left:20px;">
									<b>Payment Method :</b>
									<select id="select-payment-method" name="payment" class=" form-control select-filter" title="">
										<option selected="selected" value="all">All Payment Method</option>
										<option value="cod">COD</option>
										<option value="transfer">Transfer</option>
									</select>
								</div>
								<div style="width:200px;float:left;margin-left:20px;">
									<b>Status Order:</b>
									<select id="select-status-order" name="status" class=" form-control" title="">
										<option selected="selected" value="">All Status</option>
										<option value="status : menunggu-konfirmasi">menunggu konfirmasi</option>
										<option value="status : konfirmasi-pesanan">konfirmasi pesanan</option>
										<option value="status : proses-pengiriman">proses pengiriman</option>
										<option value="status : pembayaran-diterima & pengiriman-selesai">pembayaran diterima & pengiriman selesai</option>
										<option value="status : menunggu-pembayaran">menunggu pembayaran</option>
										<option value="status : pembayaran-sukses">pembayaran sukses</option>
										<option value="status : pesanan-diterima">pesanan diterima</option>
									</select>
								</div>
							</div>
							<div class="col-sm-1"></div>
						</div>
						<div id="main-load"></div>
						<div 
							id="loader-bar"
							style="width: 50%;margin: 0 auto;height: 15px;border-radius: 10px;margin-top: 20px;max-width: 250px;" 
							class="progress-bar bg-info progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"
							>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title mt-0" id="myLargeModalLabel">Batch Update Status Order</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div id="batch-modal-body2" class="modal-body">
				...
			</div>
		</div>
	</div>
</div>
<div class="modal fade bs-modal-resi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title mt-0" id="myLargeModalLabel">Batch Update Status Resi</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div id="batch-modal-body-resi" class="modal-body">
				...
			</div>
		</div>
	</div>
</div>