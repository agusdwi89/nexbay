<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage Gallery</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Edit Gallery Section</h4>
                        <br>
                        <?=form_open('',array("class"=>"form-horizontal"))?>
                            <div class="form-group row">
                                <label class="col-md-12 control-label">Button Text</label>
                                <div class="col-md-12">
                                    <input type="text" name="ar[button_text]" class="form-control" value="<?=$master->button_text;?>" placeholder="Button text">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-12 control-label">Button Type</label>
                                <div class="col-md-12">
                                    <select id="select-type" name="ar[button_type]" class=" form-control" title="">
                                        <option <?=(("section" == $master->button_type)?"selected='selected'":"");?> value="section">Section</option>
                                        <option <?=(("external" == $master->button_type)?"selected='selected'":"");?> value="external">External</option>
                                    </select>                                
                                </div>
                            </div>
                            <div id="state-section" class="form-group row state-option">
                                <label class="col-md-12 control-label">Section Link</label>
                                <div class="col-md-12">
                                    <select name="state[section]" class=" form-control" title="">
                                        <?php foreach ($sectionopt->result() as $s): ?>
                                            <option <?=(($s->section_name == $master->button_link)?"selected='selected'":"");?>><?=$s->section_name;?></option>
                                       <?php endforeach ?>
                                    </select>                                
                                </div>
                            </div>
                            <div id="state-external" class="form-group row state-option">
                                <label class="col-md-12 control-label">External URL</label>
                                <div class="col-md-12">
                                    <input type="text" name="state[external]" class="form-control" value="<?=$master->button_link;?>" placeholder="Button text">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <button id="btn-backto-section" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Back to Section</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Add Gallery Photo</h4>
                        <br>
                        <?=form_open_multipart('manage/section_gallery/add_item',array("class"=>"form-horizontal"))?>
                            <input type="hidden" name="sgal_id" value="<?=$id;?>"> 
                            <div class="form-group row">
                                <label class="control1-label col-md-2">Photo</label>
                                <div class="col-md-12">
                                    <input type="file" class="default" name="userfile[]">
                                    <p class="text-muted m-b-25">* Image size up to 660 x 660 PX , JPG & PNG allowed.</p>
                                </div>
                            </div>
                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <button id="btn-reload" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Clear</button>
                        <?=form_close()?>
                        <br>
                        <table class="table table-space m-0">   
                            <thead>
                                <tr>
                                    <th>Photo</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?foreach ($items->result() as $k):?>
                                    <tr>
                                        <td>
                                            <a href="<?=base_url()?>assets/section/<?=$k->image;?>" target="_blank">
                                                <img width=100 src="<?=base_url()?>assets/section/<?=$k->image;?>">
                                            </a>
                                        </td>
                                        <td>
                                            <center>
                                                <a style="opacity:100 !important;margin-right:12px" title="delete" href="<?=base_url()?>manage/section_gallery/delete_item/<?=$k->id;?>/<?=$id;?>" class="fa fa-times-circle delete-list dz-tip confirm-delete"></a>
                                            </center>
                                        </td>
                                    </tr>
                                <?endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $(".state-option").hide();
        $( "#select-type" ).change(function(e) {
           if ($(this).val() == "section") {
                $("#state-external").hide();
                $("#state-section").show();
            }else{
                $("#state-section").hide();
                $("#state-external").show();
            }
        });
        $( "#select-type" ).trigger('change');
    })
</script>