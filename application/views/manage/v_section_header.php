<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage Header</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Edit Header Image</h4>
                        <br>
                        <?=form_open_multipart(base_url('manage/section_header/edit_photo/'.$this->uri->segment(4)),array("class"=>"form-horizontal"))?>
                        <div class="form-group row">
                            <label class="control-label col-md-2">image</label>
                            <div class="col-md-10">
                                <?if ($head->image != ""): ?>
                                <img width="100" src="<?=base_url()?>assets/section/<?=$head->image;?>">
                                <?endif;?>
                                <input type="file" class="default" name="userfile">
                                <br>
                                <p>* Image size 996 x 661 px , JPG & PNG allowed.</p>
                                <button style="margin-top:-10px !important" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            </div>
                        </div>
                        <?=form_close()?>
                        <br>
                        <?=form_open_multipart(base_url('manage/section_header/edit_photo2/'.$this->uri->segment(4)),array("class"=>"form-horizontal"))?>
                        <div class="form-group row">
                            <label class="control-label col-md-2">image background</label>
                            <div class="col-md-10">
                                <?if ($head->image_bg != ""): ?>
                                <img width="100" src="<?=base_url()?>assets/section/<?=$head->image_bg;?>">
                                <?endif;?>
                                <input type="file" class="default" name="userfile">
                                <br>
                                <p>* Image size 2000 x 1080 px , JPG & PNG allowed.</p>
                                <button style="margin-top:-10px !important" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            </div>
                        </div>
                        
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Add Header Title Slide</h4>
                        <br>
                        <?=form_open_multipart('',array("class"=>"form-horizontal"))?>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Title First</label>
                                <div class="col-md-10">
                                    <input type="text" placeholder="title first" class="form-control" name="ar[title_first]" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Title Highlight</label>
                                <div class="col-md-10">
                                    <input type="text" placeholder="title first" class="form-control" name="ar[title_highlight]" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Title Last</label>
                                <div class="col-md-10">
                                    <input type="text" placeholder="title first" class="form-control" name="ar[title_last]" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Button Text</label>
                                <div class="col-md-10">
                                    <input type="text" name="ar[button_text]" class="form-control" value="" placeholder="Button text">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Button Type</label>
                                <div class="col-md-10">
                                    <select id="select-type" name="ar[button_type]" class=" form-control" title="">
                                        <option value="section">Section</option>
                                        <option value="external">External</option>
                                    </select>                                
                                </div>
                            </div>
                            <div id="state-section" class="form-group row state-option">
                                <label class="col-md-2 control-label">Section Link</label>
                                <div class="col-md-10">
                                    <select name="state[section]" class=" form-control" title="">
                                        <?php foreach ($sectionopt->result() as $s): ?>
                                            <option><?=$s->section_name;?></option>
                                        <?php endforeach ?>
                                    </select>                                
                                </div>
                            </div>
                            <div id="state-external" class="form-group row state-option">
                                <label class="col-md-2 control-label">External URL</label>
                                <div class="col-md-3">
                                    <input type="text" name="state[external]" class="form-control"  placeholder="Button text">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">JS Function</label>
                                <div class="col-md-6">
                                    <input type="text" name="ar[button_script]" class="form-control"  placeholder="Button text">
                                    <p>*beware if you wrong, can break the website entirely</p>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <button id="btn-backto-section" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Back to Section</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Edit Header Section</h4>
                        <table class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Title</th>
                                    <th>Link</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?$i=0;foreach ($items->result() as $v): $i++;?>
                                    <tr>
                                        <td><?=$i;?></td>
                                        <td>
                                            <?=$v->title_first;?><br>
                                            <b><?=$v->title_highlight;?></b><br>
                                            <?=$v->title_last;?><br>
                                        </td>
                                        <td>
                                            <?if ($v->button_type == "section"): ?>
                                                <a target="_blank" href="<?=base_url()?>#<?=$v->button_link;?>"><?=$v->button_text;?></a>
                                            <?else:?>
                                                <a target="_blank" href="<?=$v->button_link;?>"><?=$v->button_text;?></a>
                                            <?endif;?>
                                        </td>
                                        <td>
                                            <center>
                                                <a style="opacity:100 !important;margin-right:12px;float:initial;" title="delete" href="<?=base_url()?>manage/section_header/delete/<?=$v->id;?>/<?=$this->uri->segment(4);?>" class="fa fa-times-circle delete-list dz-tip confirm-delete"></a>
                                            </center>
                                        </td>
                                    </tr>
                                <?endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $(".state-option").hide();
        $( "#select-type" ).change(function(e) {
           if ($(this).val() == "section") {
                $("#state-external").hide();
                $("#state-section").show();
            }else{
                $("#state-section").hide();
                $("#state-external").show();
            }
        });
        $( "#select-type" ).trigger('change');
    })
</script>