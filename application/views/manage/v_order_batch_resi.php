<div class="row">
	<div class="col-12 text-center">
		<h4>Import Batch Order Resi</h4>
		<b>Date <?=format_date_time($data['s'],false);?> ~ <?=format_date_time($data['e'],false);?></b>
	</div>
</div>
<div class="row">
	<div class="col-5 text-center">
		<br><br><br>
		<a href="<?=base_url()?>manage/order/batch_download_resi/<?=$data['s'];?>s<?=$data['e'];?>/<?=$data['p'];?>" title="show customer payment confirmation" class="btn-confirm-download btn btn-success waves-effect waves-light btn-sm dz-tip" target="_blank"> 
			<i class="fa fa-download m-r-5"></i> 
			<span>download<br>template</span> 
		</a>
		<br><br><br>
	</div>
	<div class="col-2 text-center" style="font-size: 6px;color: #000000;line-height: 12px;">
		<br><br>
		|<br>|<br>|<br>|<br>|<br>|<br>|<br>|<br>|<br>|<br>|<br>|<br>|
	</div>
	<div class="col-5 text-center">
		<br><b>upload order resi(.csv)</b><br><br>
		<?=form_open_multipart(base_url('manage/order/batch_resi_upload'))?>
		 	<input type="file" class="default" name="userfile">
		 	<button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
		 	<br><br>
		 	<div style="text-align:left">
		 		<small>*download & follow template to batch resi update</small><br>
		 		<small>*change only status value on the right column</small>
		 	</div>
		<?=form_close()?>
	</div>
</div>
