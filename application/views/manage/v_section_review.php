<script type="text/javascript">
    $(function(){
        $(".state-option").hide();
        $( "#select-type" ).change(function(e) {
           if ($(this).val() == "section") {
                $("#state-external").hide();
                $("#state-section").show();
            }else{
                $("#state-section").hide();
                $("#state-external").show();
            }
        });
        $( "#select-type" ).trigger('change');
    })
</script>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage Highlight & Review</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Edit Highlight & Review Section</h4>
                        <br>
                        <?=form_open_multipart('',array("class"=>"form-horizontal"))?>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Title Left</label>
                                    <div class="col-md-9">
                                        <input type="text" placeholder="title first" class="form-control" name="title_left" value="<?=$master->title_left;?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Description Left</label>
                                    <div class="col-md-9">
                                        <input type="text" placeholder="title first" class="form-control" name="description_left" value="<?=$master->description_left;?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Title Right</label>
                                    <div class="col-md-9">
                                        <input type="text" placeholder="title first" class="form-control" name="title_right" value="<?=$master->title_right;?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Description Right</label>
                                    <div class="col-md-9">
                                        <input type="text" placeholder="title first" class="form-control" name="description_right" value="<?=$master->description_right;?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-md-3">image</label>
                                    <div class="col-md-9">
                                        <?if ($master->image != ""): ?>
                                        <a target="_blank" href="<?=base_url()?>assets/section/<?=$master->image;?>">
                                            <img class="image-section-header-placeholder" src="<?=base_url()?>assets/section/<?=$master->image;?>">
                                        </a>
                                        <span>change image : </span>
                                        <?endif;?>
                                        <input type="file" class="default" name="image">
                                        <br>
                                        <p class="text-muted m-b-25">* Image size 913 x 418 px , JPG & PNG allowed.</p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-md-3">image Logo</label>
                                    <div class="col-md-9">
                                        <?if ($master->image_logo != ""): ?>
                                        <a target="_blank" href="<?=base_url()?>assets/section/<?=$master->image_logo;?>">
                                            <img class="image-section-header-placeholder" src="<?=base_url()?>assets/section/<?=$master->image_logo;?>">
                                        </a>
                                        <span>change image : </span>
                                        <?endif;?>
                                        <input type="file" class="default" name="image_logo">
                                        <br>
                                        <p class="text-muted m-b-25">* Image size 200 x 200 px , JPG & PNG allowed.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Title Bottom</label>
                                    <div class="col-md-9">
                                        <input type="text" placeholder="title first" class="form-control" name="title_bottom" value="<?=$master->title_bottom;?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Title Bottom Highlight</label>
                                    <div class="col-md-9">
                                        <input type="text" placeholder="title first" class="form-control" name="title_bottom_highlight" value="<?=$master->title_bottom_highlight;?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Description Bottom</label>
                                    <div class="col-md-9">
                                        <input type="text" placeholder="title first" class="form-control" name="description" value="<?=$master->description;?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Button Text</label>
                                    <div class="col-md-7">
                                        <input type="text" name="ar[button_text]" class="form-control" value="<?=$master->button_text;?>" placeholder="Button text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Button Type</label>
                                    <div class="col-md-7">
                                        <select id="select-type" name="ar[button_type]" class=" form-control" title="">
                                            <option <?=(("section" == $master->button_type)?"selected='selected'":"");?> value="section">Section</option>
                                            <option <?=(("external" == $master->button_type)?"selected='selected'":"");?> value="external">External</option>
                                        </select>                                
                                    </div>
                                </div>
                                <div id="state-section" class="form-group row state-option">
                                    <label class="col-md-3 control-label">Section Link</label>
                                    <div class="col-md-7">
                                        <select name="state[section]" class=" form-control" title="">
                                            <?php foreach ($sectionopt->result() as $s): ?>
                                                <option <?=(($s->section_name == $master->button_link)?"selected='selected'":"");?>><?=$s->section_name;?></option>
                                            <?php endforeach ?>
                                        </select>                                
                                    </div>
                                </div>
                                <div id="state-external" class="form-group row state-option">
                                    <label class="col-md-3 control-label">External URL</label>
                                    <div class="col-md-7">
                                        <input type="text" name="state[external]" class="form-control" value="<?=$master->button_link;?>" placeholder="Button text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 control-label">JS Function</label>
                                    <div class="col-md-7">
                                        <input type="text" name="ar[button_script]" class="form-control" value="<?=$master->button_script;?>" placeholder="Button text">
                                        <small>*beware if you wrong, can break the website entirely</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                                <button id="btn-backto-section" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Back to Section</button>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Add Partners Logo</h4>
                        <br>
                        <?=form_open_multipart('manage/section_review/add_customer/'.$master->id,array("class"=>"form-horizontal"))?>
                            <input type="hidden" name="srev_id" value="<?=$id;?>"> 
                            <div class="form-group row">
                                <label class="control-label col-md-2">Logo</label>
                                <div class="col-md-10">
                                    <input type="file" class="default" name="userfile">
                                    <p class="text-muted m-b-25">* Image size up to 200 x 45 PX , JPG & PNG allowed.</p>
                                </div>
                            </div>
                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <button id="btn-reload" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Clear</button>
                        <?=form_close()?>
                        <br>
                        <div class="row">
                            <?foreach ($items->result() as $i): ?>
                                <div class="col-lg-2">
                                    <img width=75 src="<?=base_url()?>assets/section/<?=$i->image;?>">
                                    <a style="opacity:100 !important;margin-right:12px" title="delete" href="<?=base_url()?>manage/section_review/delete_partners/<?=$i->id;?>/<?=$id;?>" class="fa fa-times-circle delete-list dz-tip confirm-delete"></a>
                                </div>
                            <?endforeach;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>