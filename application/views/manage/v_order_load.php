<script type="text/javascript">
	$(function(){
		$('#datatable').dataTable({
			"initComplete": function(settings, json) {
				$("#loader-bar").hide();
				$("#datatable_wrapper").css('opacity', '1');
				$("#datatable").css('opacity', '1');
				$('.dz-tip-load').tooltipster();
			}
		});

		$('.bs-modal-cancel-refund').on('show.bs.modal', function(e) { 
			$("#mdl_id").val($(e.relatedTarget).data('id'));
			$("#mdl_from").html($(e.relatedTarget).data('from'));
			$("#mdl_product").html($(e.relatedTarget).data('product'));
		}) ;
	})
</script>
<table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
	<thead>
		<tr>
			<th>No</th>
			<th>Customer Identity</th>
			<th>Product</th>
			<th>Price</th>
			<th class="text-center">Order Detail</th>
			<th class="text-center">Action</th>
		</tr>
	</thead>
	<tbody>
		<?$i=0;foreach ($db as $d): $i++;?>
			<tr>
				<td><?=$i;?></td>
				<td>
					<b><?=$d->cust_f_name;?></b><br>
					e : <?=$d->cust_email;?><br>
					h : <?=$d->cust_phone;?><hr>
					<p class="txwrap"><?=$d->cust_address;?></p>
					<?=$d->province;?><br><?=$d->city;?><br><?=$d->cust_zip?>
				</td>
				<td>
					<img width="50" src="<?=base_url()?>assets/section/<?=$d->prd_image;?>">
					<div>
						<b><?=$d->prd_title;?></b>
						<p style="margin-bottom: 0.1rem"><?=$d->prd_description;?></p>
						<p style="margin-bottom: 0.1rem">Rp. <?=format_number($d->prd_price);?></p>
						<span class="badge badge-purple">subdomain : <?=$d->subdomain;?></span>
					</div>
				</td>
				<td>
					<h4>Rp. <?=format_number($d->prd_total);?></h4>
					<b>Qty : <?=$d->order_quantity?> pcs</b><br>
					<b>item: Rp. <?=format_number($d->prd_price * $d->order_quantity)?></b><br>
					<b>shipping: Rp. <?=format_number($d->prd_shipment);?></b><br>
					<?if ($d->prd_coupon > 0): ?>
						<b>coupon (-): Rp. <?=format_number($d->prd_coupon)?></b><br>
					<?endif;?>
				</td>
				<td class="text-center">
					<?=format_date_time($d->time_order);?><br>
					order ID : <b><?="#NEX1".sprintf('%05d', $d->id);?></b><br>
					<br>status :	<br>
					<?if ($d->order_status == "wait-payment"): ?>
						<?php if ($d->payment == "cod"): ?>
							<span class="badge badge-danger">menunggu-konfirmasi</span>    
						<?php else: ?>
							<span class="badge badge-danger">menunggu-pembayaran</span>
						<?php endif ?>
					<?elseif($d->order_status == "payment-success"):?>
						<?php if ($d->payment == "cod"): ?>
							<span class="badge badge-info">konfirmasi-pesanan</span>    
						<?php else: ?>
							<span class="badge badge-info">pembayaran-sukses</span>
						<?php endif ?>
					<?elseif($d->order_status == "delivery-process"):?>
						<span class="badge badge-info">proses-pengiriman</span>
					<?elseif($d->order_status == "cancel"):?>
						<span class="badge badge-warning">order-di-cancel</span>
					<?elseif($d->order_status == "refund"):?>
						<span class="badge badge-warning">order-di-refund</span>
					<?else: ?>
						<?php if ($d->payment == "cod"): ?>
							<span class="badge badge-success">pembayaran-diterima & pengiriman-selesai</span>    
						<?php else: ?>
							<span class="badge badge-success">pesanan-diterima</span>
						<?php endif ?>
					<?endif;?>
					<?if (strlen($d->cancel_refund_reason) > 0): ?>
						<br><br>
						<b>cancel / refund reason : </b><br>
						<span><?=$d->cancel_refund_reason;?></span>
					<?endif;?>
					<br><br>payment method :	<br>
					<span class="badge badge-purple"><?=$d->payment;?></span>
				</td>
				<td>
					<br>
					<div class="btn-group m-b-10" style="display: block;margin: 0 auto;text-align: center;">
						<a href="<?=base_url()?>manage/order/detail/<?=$d->id;?>" class="btn btn-sm btn-icon waves-effect waves-light btn-purple dz-tip-load" title="Detail Info"> <i class="fa fa-arrow-circle-right"></i> </a>
						<a target="_blank" href="<?=base_url()?>home/order/<?=$d->link_unique;?>" class="btn btn-sm btn-icon waves-effect waves-light btn-purple dz-tip-load" title="Show on Customer Page"> <i class="fa fa-search"></i> </a>
						<a 
							data-id = "<?=$d->id;?>" data-from="<?=$d->cust_f_name;?> (<?=$d->cust_email;?>)" data-product="<?=$d->prd_title;?>"
							data-toggle="modal" data-target=".bs-modal-cancel-refund" href="#" class="btn btn-sm btn-icon waves-effect waves-light btn-warning dz-tip-load pop-cancel-refund" title="Refund / Cancel Order"> <i class="fa fa-exclamation-triangle"></i> </a>
						<a href="<?=base_url()?>manage/order/delete/<?=$d->id;?>" class="btn btn-sm btn-icon waves-effect waves-light btn-danger dz-tip-load confirm-delete" title="Delete Order"> <i class="fa fa-trash"></i> </a>
						<?php if ($d->receipt != ""): ?>
							<br><br>
							<a href="<?=base_url()?>assets/receipt/<?=$d->receipt;?>"
								title="show customer payment confirmation"
								class="btn-confirm-download btn btn-success waves-effect waves-light btn-sm dz-tip-load" target="_blank"> 
								<i class="fa fa-file-image-o m-r-5"></i> 
								<span>payment<br>confirmation</span> 
							</a>													
						<?php endif ?>
						
					</div>
				</td>
			</tr>
		<?endforeach;?>
	</tbody>
</table>

<div class="modal fade bs-modal-cancel-refund" tabindex="-1" role="dialog" aria-labelledby="modal-cancel-refund" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title mt-0" id="modal-cancel-refund">Cancel / Refund Order</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div id="batch-modal-body" class="modal-body">
				<?=form_open(base_url('manage/order/cancel_refund'),array('class'=>'form-horizontal','id'=>'mdl-form-cancel-refund'))?>
					<input id="mdl_id" type="hidden" name="id" value="">
					<div class="form-group row">
						<label class="col-md-2 control-label">Order From</label>
						<div class="col-md-10">
							<b id="mdl_from">LLSAJD laksjd (askjdaskd@asldkjsad.com)</b><br>
							<span id="mdl_product">Smart Back Pack</span>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-2 control-label">Status</label>
						<div class="col-md-3">
							<select name="order_status" class="form-control">
								<option>cancel</option>
								<option>refund</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-2 control-label">Description</label>
						<div class="col-md-10">
							<textarea placeholder="Cancelation / Refund Reason" class="form-control" rows="3" name="cancel_refund_reason"></textarea>
						</div>
					</div>
					<button id="mdl-submit-cancel-refund" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
					<button id="mdl-cancel-refund-close" type="button" data-dismiss="modal" class="btn btn-secondary waves-effect waves-light m-t-20">Cancel</button>
				<?=form_close()?>
				<div id="mdl-form-loading" style="display:none">
					<br><br><h3>Loading . . .</h3>
				</div>
			</div>
		</div>
	</div>
</div>