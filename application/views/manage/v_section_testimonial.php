<script type="text/javascript">
    $(function(){
        $(".state-option").hide();
        $( "#select-type" ).change(function(e) {
           if ($(this).val() == "section") {
                $("#state-external").hide();
                $("#state-section").show();
            }else{
                $("#state-section").hide();
                $("#state-external").show();
            }
        });
        $( "#select-type" ).trigger('change');
    })
</script>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage Testimonial</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Edit Testimonial</h4>
                        <br>
                        <?=form_open('',array("class"=>"form-horizontal"))?>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label class="col-md-12 control-label">Title Left</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="title first" class="form-control" name="title_first" name="title" value="<?=$master->title_first;?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-12 control-label">Title Left</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="title first" class="form-control" name="title_highlight" name="title" value="<?=$master->title_highlight;?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-12 control-label">Title Last</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="title first" class="form-control" name="title_last" name="title" value="<?=$master->title_last;?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-12 control-label">Number</label>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="title first" class="form-control" name="number" name="title" value="<?=$master->number;?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-12 control-label">Description Left</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="title first" class="form-control" name="description" value="<?=$master->description;?>">
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                                <button id="btn-backto-section" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Back to Section</button>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Add Review</h4>
                        <br>
                        <?=form_open_multipart('manage/section_testimonial/add_testi/'.$master->id,array("class"=>"form-horizontal"))?>
                            <input type="hidden" name="testi_id" value="<?=$id;?>"> 
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Name</label>
                                <div class="col-md-8">
                                    <input type="text" placeholder="title first" class="form-control" name="name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Review</label>
                                <div class="col-md-10">
                                    <input type="text" placeholder="Review" class="form-control" name="review">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">Picture</label>
                                <div class="col-md-10">
                                    <input type="file" class="default" name="userfile">
                                    <p class="text-muted m-b-25">* Image size up to 200 x 45 PX , JPG & PNG allowed.</p>
                                </div>
                            </div>
                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                            <button id="btn-reload" type="submit" class="btn btn-secondary waves-effect waves-light m-t-20">Clear</button>
                        <?=form_close()?>
                        <br>
                        <div class="row">
                            <table class="table table-space m-0">   
                                <thead>
                                    <tr>
                                        <th>Photo</th>
                                        <th>Review</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?foreach ($items->result() as $k):?>
                                    <tr>
                                        <td>
                                            <a href="<?=base_url()?>assets/section/<?=$k->photo;?>" target="_blank">
                                                <img width=100 src="<?=base_url()?>assets/section/<?=$k->photo;?>">
                                            </a>
                                        </td>
                                        <td>
                                            <b><?=$k->name;?></b><br>
                                            <span><?=$k->position;?></span>
                                            <p><?=$k->review;?></p>
                                        </td>
                                        <td>
                                            <center>
                                                <a style="opacity:100 !important;margin-right:12px" title="delete" href="<?=base_url()?>manage/section_testimonial/delete_testi/<?=$k->id;?>/<?=$id;?>" class="fa fa-times-circle delete-list dz-tip confirm-delete"></a>
                                            </center>
                                        </td>
                                    </tr>
                                    <?endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>