<!DOCTYPE html>
<html lang="en">
	<head>
		<?=$this->load->view('fe/header');?>	
		<script type="text/javascript">
			var clog;
		</script>
	</head>

<body>
	<?=form_open('/',array('id'=>'global-form'))?><?=form_close()?>
	<div class="menu-fix">
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-sm-6">
					<a class="logo" href="#">
						<img class="img-fluid" src="<?=base_url()?>assets/geniot/images/logo.png" alt="logo">
					</a>
				</div>
				<div class="col-sm-6 text-right re-mob">				
					<a id="button-buy-now-top" class="button" href="<?=base_url()?>" role="button" style="padding: 2px 26px;margin-right: 31px;font-size: 12px;margin-top: -2px;" title="buy now">back to homepage</a>
				</div>
			</div>
		</div>
	</div>
	<!-- Start Content -->
	<section id="main" class="wrapper" style="padding:0px">
		<section id="products-2" class="products block gray">
			<div class="container">
				<div class="row">
					<div class="col-md-12" style="text-align:center">
						<h2><span class="hight-text">Order</span>Detail</h2>
						<p>order id : #NEX1<?=sprintf("%'.05d\n", $db->id);?></p>
					</div>
					<div class="block-desc"></div>
					<br>
					<?
						$d['db'] 		= $db;
						$this->load->view('fe/invoice_front',$d);
					?>
				</div>
			</div>
		</section>
	</section>

	<!-- End Content -->
	<?=$this->load->view('fe/footer_script');?>
	<?=$this->load->view('fe/i_popup');?>

	<?=$this->load->view('fe/ga');?>

</html>