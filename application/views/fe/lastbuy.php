<link rel="stylesheet" type="text/css" href="<?=base_url('assets/geniot/css/last_buy.css')?>">
<script src="<?=base_url()?>script/lastbuy"></script>

<div style="display:none" id="call_buy_wrapper" class="ng-tns-c9-3 proof-factor-notification-box ng-trigger ng-trigger-fadeInOut ng-star-inserted proof-factor-is-desktop-bottom proof-factor-is-desktop-left proof-factor-is-playground-preview proof-factor-is-shape-rounded proof-factor-is-clickable proof-factor-is-type-recent-activity proof-factor-is-mask-rounded-square proof-factor-is-icon-type-image">
	<div class="proof-factor-notification">	
		<div class="proof-factor-icon-box">
			<img id="call_buy_img" alt="" class="proof-factor-icon ng-star-inserted" src="https://bluetoothspeaker.nexbay.id/assets/section/63c9aa316b1aae6fa49944467750b87f.png" style="">
		</div>
		<div class="proof-factor-body">
			<div id="call_buy_from" class="proof-factor-title">
				Nur Widia Dari Alam Sutera, Tangerang
			</div>
			<div id="call_buy_prd" class="proof-factor-text ng-star-inserted" style="">Beli JBL GO 2 Warna Merah</div>
			<div class="proof-factor-details ng-star-inserted" style="">
				<div id="call_buy_sec" class="proof-factor-details-text">3 minutes ago</div>
				<a class="proof-factor-powered-link" href="https://prooffactor.com" target="_blank">
					<i class="proof-factor-powered-icon">
						<svg class="ng-tns-c9-3" height="13" viewBox="0 0 7 13" width="7" xmlns="http://www.w3.org/2000/svg">
							<g class="ng-tns-c9-3" fill="none" fill-rule="evenodd">
								<path class="ng-tns-c9-3" d="M4.127.496C4.51-.12 5.37.356 5.16 1.07L3.89 5.14H6.22c.483 0 .757.616.464 1.044l-4.338 6.34c-.407.595-1.244.082-1.01-.618L2.72 7.656H.778c-.47 0-.748-.59-.48-1.02L4.13.495z" fill="#F6A623">
								</path>
								<path class="ng-tns-c9-3" d="M4.606.867L.778 7.007h2.807l-1.7 5.126 4.337-6.34H3.16" fill="#FEF79E">
								</path>
							</g>
						</svg>
					</i>
					<span class="proof-factor-powered-proof-factor">ProofFactor</span>
				</a>
			</div>
		</div>
	</div>
</div>