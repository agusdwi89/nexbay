<?
	$btn_section_id = $this->button_script_id;
	$dbs = array();
	foreach ($btn_section_id as $key => $value) {
		$ids = implode(",", $btn_section_id[$key]);
		$dbs[$key] = $this->db->query("select id,button_script from $key where id in ( $ids )");
	}
	$script = array();
	foreach ($dbs as $k => $v) {
		foreach ($v->result() as $s) {
			$script[] = array(
				'section'	=> $k,
				'id'		=> $s->id,
				'value'		=> $s->button_script
			);
		}
	}
?>

<script type="text/javascript">
	$(function(){
		<?php foreach ($script as $v): ?>
			<?php if(strlen($v['value']) > 0):?>
		$("#btn_<?=$v['section'];?>_<?=$v['id'];?>").on('click', function () {<?=$v['value'];?>});
			<?php endif;?>
		<?php endforeach ?>
})
</script>


