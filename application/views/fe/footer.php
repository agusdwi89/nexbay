<footer class="iq-pt-50">
	<div class="container-fluid">
		<div class="row">
			<hr class="iq-mt-60">
			<div class="col-sm-12 text-center">
				<div class="footer-copyright iq-pt-20 iq-pb-20">© Copyright 2018 Geniot Developed by <a target="_blank" href="https://iqonicthemes.com/">iqonicthemes</a>.</div>
			</div>
		</div>
	</div>
</footer>