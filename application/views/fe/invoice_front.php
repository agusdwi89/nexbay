<style type="text/css">
    input{
        width: 100%;
        padding: 5px;
        height: auto;
        border-radius: 5px;
        font-size: 10px;
        line-height: 22px;
        /* text-align: right; */
        border-color: #f3c000;
    }
    button{
        margin: 0 auto !important;
        display: block !important;
        padding: 9px 19px !important;
        width: auto !important;
        height: auto !important;
        font-size: 13px !important;
        font-weight: bold !important;
        border-radius: 10px !important;
    }
    #upload-receipt img{
        width: 200px;
        display: block;
        margin: 15px auto 22px;
        border-radius: 5px;
        border: 4px solid #e1e2e1;
    }
</style>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h5 class="panel-title">Ringkasan Pesanan</hh5>
                    </div>
                    <div class="panel-body">
                        <div class="card">
                            <div class="card-body">
                                <div class="row m-t-30">
                                    <div class="col-md-7">
                                        <div >
                                            <p style="margin-bottom:5px"><small><strong>Order Date: </strong></small> <?=pretty_date($db->time_order);?></p>
                                            <p style="margin-bottom:5px"><small><strong>Order ID: </strong></small> #NEX1<?=sprintf("%'.05d\n", $db->id);?></p>
                                            <div style="margin-bottom:5px">
                                                <small><strong>Order Status: </strong></small> 

                                                <?if($db->order_status == "cancel"):?>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-warning">order di cancel</span></a>
                                                <?elseif($db->order_status == "refund"):?>
                                                    <a ><span style="padding: 5px 7px;" class="badge badge-warning">order di refund</span></a>
                                                <?else: ?>
                                                    <?if ($db->order_status == "wait-payment"): ?>
                                                        <?php if ($db->payment == "cod"): ?>
                                                            <span class="badge badge-danger">Menunggu Konfirmasi</span>    
                                                        <?php else: ?>
                                                            <span class="badge badge-danger">Menunggu Pembayaran</span>
                                                        <?php endif ?>
                                                    <?elseif($db->order_status == "payment-success"):?>
                                                        <?php if ($db->payment == "cod"): ?>
                                                            <span class="badge badge-info">Konfirmasi Pesanan</span>    
                                                        <?php else: ?>
                                                            <span class="badge badge-info">Pembayaran Sukses</span>
                                                        <?php endif ?>
                                                    <?elseif($db->order_status == "delivery-process"):?>
                                                        <span class="badge badge-info">Proses Pengiriman</span>
                                                    <?else: ?>
                                                        <span class="badge badge-success">Pesanan diterima</span>
                                                    <?endif;?>
                                                <?endif;?>
                                            </div>
                                        </div>
                                        <?if($db->resi != ''):?>
                                            <div id="resi-delivery">
                                                <p style="margin-bottom:5px">
                                                    <small><strong>No Resi : </strong></small> 
                                                    <?=$db->resi;?>
                                                </p>
                                            </div>
                                        <?endif?>
                                    </div>
                                    <div class="col-md-5">
                                        <b>Shipping Address</b>
                                        <address class="line-h-24" style="font-size: 11px;line-height: 20px;">
                                            <b style="font-size: 13px;text-transform: capitalize;"><?=$db->cust_f_name;?></b><br>
                                            <?=$db->cust_address;?><br>
                                            <?=$db->city;?><br><?=$db->province;?><br>
                                            Kec. <?=$db->cust_kecamatan;?> - <?=$db->cust_zip;?><br>
                                            <abbr title="Phone">P:</abbr> <?=$db->cust_phone;?>
                                        </address>
                                    </div>
                                </div>
                                <br><br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table m-t-30">
                                                <thead>
                                                    <tr><th>#</th>
                                                        <th>Picture</th>
                                                        <th>Item</th>
                                                        <th>Description</th>
                                                        <th class="text-right">Price</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>
                                                            <img width="75" src="<?=base_url()?>assets/section/<?=$db->prd_image;?>">
                                                        </td>
                                                        <td>
                                                            <b><?=$db->title;?></b><br>
                                                            <?php if ($db->order_quantity > 1): ?>
                                                                <br>QTY : <?=$db->order_quantity;?> pcs
                                                            <?php endif ?>
                                                        </td>
                                                        <td>
                                                            <?=$db->description;?><br>
                                                            <?=$db->description2;?>
                                                        </td>
                                                        <td class="text-right">Rp. <?=format_number($db->prd_price);?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6">
                                        <div class="float-right" style="float:right;text-align:right">
                                            <?if ($db->prd_coupon > 0): ?>
                                            <p><b>Coupon (<?=$db->prd_coupon_code;?>) :</b> Rp. <?=format_number($db->prd_coupon);?> (-)</p>
                                            <?endif;?>
                                            <p><b>Shipping via <?=$db->cust_shipping;?> <?=format_number($db->prd_shipment)?></b></p>
                                            <h3>Rp. <?=format_number($db->prd_total);?></h3>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div id="pay-sum-bank" class="paysum" style="display: block;">
                    <div class="card text-white bg-warning">
                        <div class="card-body">
                            <b>Informasi Pembayaran :</b>
                            <br><br>
                            <b><?=$this->db->get_where('mst_payment',array('code'=>$db->payment))->row()->name;;?> </b>
                            <p>
                                <?php if ($db->payment == "cod"): ?>
                                    <p><?=$this->db->get_where('mst_payment',array('id'=>1))->row()->description;?></p>
                                <?php elseif($db->payment == "transfer"): ?>
                                    <p><?=$this->db->get_where('mst_payment',array('id'=>2))->row()->description;?></p>
                                    <b style="margin-bottom: 8px;display: block;"><?=$db->payment_detail;?></b>
                                <?php else: ?>
                                    <p><?=$this->db->get_where('mst_payment',array('code'=>$db->payment))->row()->description;?></p>
                                <?php endif ?>
                            </p>

                            <?if (!(strpos($db->payment,"doku") === false)): ?>
                                <?if ($db->order_status == "wait-payment"): ?>
                                    <br>
                                    <b>Pay With Doku</b>
                                    <a id="dokuPopUp" class="select_payment2 doku" href="<?=base_url()?>doku/pay/<?=$db->link_unique;?>">
                                        <div class="col-md-3">
                                            <img src="https://nexbay.id/assets/images/doku.png" style="width:!00px">
                                        </div> 
                                        <div style="font-size:11px !important;text-align:left" class="col-md-9 ">Bayar Pakai Doku</div>
                                        <div class="clearfix"></div>
                                    </a>       
                                    <script type="text/javascript">
                                        paymentStart = "doku";
                                    </script>
                                <?endif;?>
                            <?endif;?>
                        </div>
                    </div>
                    <br>
                    <?php if ($db->payment == "transfer"): ?>
                        <div id="upload-receipt" class="card text-white bg-warning">
                            <div class="card-body">
                                <b>Unggah bukti pembayaran :</b>
                                <br>
                                <?php if ($db->receipt != ""): ?>
                                    <a target="_blank" href="<?=base_url()?>assets/receipt/<?=$db->receipt;?>">
                                        <img src="<?=base_url()?>assets/receipt/<?=$db->receipt;?>">    
                                    </a>
                                <?php endif ?>
                                <?=form_open_multipart(base_url('home/payment_confirm/'.$db->id.'/'.$db->link_unique));?>
                                    <input type="file" name="userfile">
                                    <button class="button action-button-nav smooth" type="submit">unggah</button>
                                <?=form_close()?>
                            </div>
                            </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div> 
</div>


<div class="container" style="margin-top:30px">
    <div class="row">
        <div class="col-md-12">
            <center>
                <p style="font-size:11px" class="copyright">
                    <?=$this->db->get_where('site_config',array('id'=>8))->row()->value;?>
                    <br>
                    &copy; Copyright NEXBAY.ID 2019
                </p>
            </center>
        </div>
    </div>
</div>
<div class="modal fade" id="modalDokuPayment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <iframe id="dokuPaymentiFrame" src=""></iframe>
            </div>
        </div>
    </div>
</div>