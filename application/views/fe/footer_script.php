<script src="<?=base_url()?>assets/geniot/js/jquery-3.3.1.min.js"></script>
<script src="<?=base_url()?>assets/geniot/js/popper.min.js"></script>
<script src="<?=base_url()?>assets/geniot/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/geniot/js/owl.carousel.min.js"></script>
<script src="<?=base_url()?>assets/geniot/js/side-menu.js"></script>
<script src="<?=base_url()?>assets/geniot/js/wow.min.js"></script>
<script src="<?=base_url()?>assets/geniot/js/main.js"></script>

<script src="<?=base_url()?>assets/fe/js/flexslider.js"></script>

<!-- timer -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700">
<link rel="stylesheet" href="<?=base_url()?>assets/plugins/timer/style.css">
<script src="<?=base_url()?>assets/plugins/timer/jquery.countdown.min.js"></script>

<script src="<?=base_url()?>assets/geniot/js/custom.js"></script>