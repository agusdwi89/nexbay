 <header>
 	<div class="container-fluid">
 		<div class="row align-items-center">
 			<div class="col-sm-6">
 				<a class="logo" href="#">
 					<img class="img-fluid" src="<?=base_url()?>assets/geniot/images/logo.png" alt="logo">
 				</a>
 			</div>
 			<div class="col-sm-6 text-right re-mob">

 				<? $topbutton = $this->db->get_where('site_config',array('name'=>'buy-now','subdomain'=>subdomain()))->row();?>

 				<?php if ($topbutton->value2 == "on"): ?>

 				<a 
 					id="button-buy-now-top"
 					class="button" href="#product" role="button" style="padding: 2px 26px;margin-right: 31px;font-size: 12px;margin-top: -2px;"
 					title="<?=$topbutton->value3;?>">
 					
 					<?=$topbutton->value3;?>
 				
 				</a>

 			<?php endif ?> 				
 				<!-- Side Menu -->
 				<div id="side-menu-icon">
 					<div id="menu"><span></span></div>
 				</div>
 				<div id="side-menu">
 					<?$dbtpm = $this->db->get_where('top_menu',array('subdomain'=>subdomain()));?>
 					<ul class="side-link">
 						<?php $i=0; foreach ($dbtpm->result() as $v): $i++;?>
 							<li class="<?=($i==1) ? 'current' : '';?>"><a href="#<?=$v->url;?>" title="<?=$v->name;?>"><?=$v->name;?></a></li>	
 						<?php endforeach ?>
 					</ul>
 					<ul class="media-box">
 						<li><a target="_blank" href="<?=$this->db->get_where('global_config',array('name'=>'instagram'))->row()->value;?>"><i class="fa fa-instagram"></i></a></li>
 						<li><a target="_blank" href="<?=$this->db->get_where('global_config',array('name'=>'twitter'))->row()->value;?>"><i class="fa fa-twitter"></i></a></li>
 						<li><a target="_blank" href="<?=$this->db->get_where('global_config',array('name'=>'facebook'))->row()->value;?>"><i class="fa fa-facebook"></i></a></li>
 					</ul>
 					<div class="side-footer">
 						<div class="side-copyright">
 							<?=$this->db->get_where('global_config',array('name'=>'copyright'))->row()->value;?>
 						</div>
 					</div>
 				</div>
 				<!-- Side Menu -->
 			</div>
 		</div>
 	</div>
 </header>