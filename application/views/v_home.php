<!doctype html>
<html lang="en">
   <head>

      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="shortcut icon" href="<?=base_url()?>assets/geniot/images/favicon.ico" />
      <?=$this->load->view('fe/title');?>
      <?=$this->load->view('fe/metas');?>
      <?=$this->load->view('fe/header');?>

      <?=$this->load->view('fe/fb_track');?> 
      
   </head>
   <body>
      <?=form_open('/',array('id'=>'global-form'))?>
      <?=form_close()?>

      <div id="loading">
         <div id="loading-center">
            <div class="boxContainer">
               <div class="box box6"></div><div class="box box7"></div><div class="box box1"></div><div class="box box2"></div><div class="box box3"></div><div class="box box4"></div><div class="box box5"></div><div class="box box8"></div><div class="box box9"></div>
            </div>
         </div>
      </div>

      <?=$this->load->view('fe/nav');?>

      <?php foreach ($list_section as $key): ?>

         <?=$this->load->view('section/'.$key['layout'],$key);?>
         
      <?php endforeach ?>

      <!-- <div id="back-to-top" style="display: block;">
         <a class="top" id="top" href="#top"> <i class="fa fa-long-arrow-up" aria-hidden="true"></i></a>
      </div> -->

      <?=$this->load->view('fe/footer_script');?>

      <?=$this->load->view('fe/ga');?>
      <?=$this->load->view('fe/purechat');?>
      <?=$this->load->view('fe/lastbuy');?>
   </body>
</html>