<?$items = $this->db->get_where('section_gallery_items',array('sgal_id'=>$data->id));?>

<div id="instagram" class="instagram">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<div class="head">
					<?if (strlen($data->button_text) > 0): ?>
						<?if ($data->button_type == "section"): ?>
							<a class="button" href="<?=base_url()?>#<?=$data->button_link;?>"><?=$data->button_text;?></a>
						<?else:?>
							<a class="button" target="_blank" href="<?=$data->button_link;?>"><?=$data->button_text;?></a>
						<?endif;?>
					<?endif;?>
				</div>
				<div class="owl-carousel instagram-item" data-items="6" data-items-laptop="5" data-items-tab="4" data-items-mobile="2" data-items-mobile-sm="1" data-margin="30" data-autoplay="true" data-loop="true" data-nav="false" data-dots="false">
					<?foreach ($items->result() as $v): ?>
						<div class="item">
							<div class="instagrambox">
								<div class="instagram-image">
									<img class="hover" src="<?=base_url()?>assets/section/<?=$v->image;?>" alt="instagram image">
									<ul class="instagram-icon"></ul>
								</div>
							</div>
						</div>
					<?endforeach;?>
				</div>
			</div>
		</div>
	</div>
</div>