<?
	$ubuy	= $this->db->get_where('site_config',array('subdomain'=>subdomain(),'name'=>'user-buy'))->row()->value;
	$useeb	= $this->db->get_where('site_config',array('subdomain'=>subdomain(),'name'=>'user-see-bottom'))->row()->value;
	$useet	= $this->db->get_where('site_config',array('subdomain'=>subdomain(),'name'=>'user-see-top'))->row()->value;

	$ubprd = $this->db->query("select prd_id,count(prd_id) as c FROM orders GROUP BY prd_id");
	$ubprda = array();
	foreach ($ubprd->result() as $u) {
		$ubprda[$u->prd_id] = $u->c;
	}
?>

<style type="text/css">
.buy-soldout{
	color: #ffffff;
	background: #ce3a3a !important;
}
.buy-soldout:hover{
	color: #ffffff;
	background-color: #8a2e2e !important;
}
.buy-soldout:before{
    background-color: #8a2e2e !important;
}

</style>

<?$items = $this->db->get_where('section_product_items',array('prd_id'=>$data->id,'active'=>'yes'));?>
<section id="product" class="products block gray smart-features">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="title-box">
					<h3 class="title"><?=$data->title;?></h3>
					<div class="v-text text-uppercase"><span>07</span></div>
				</div>
			</div>
			<div class="col-lg-6">
				<p><?=$data->description;?></p>
			</div>
		</div>
		<div class="row">
				<?
					$item = $items->num_rows();
					$totbaris = (intdiv(($item-1),3)+1);
					$highlight = 3 * ($totbaris-1) + 1;
					
					$offset = 0;
					$sisa = ($item % 3);
					if ($sisa > 0 ){
						$offset = 1 / $sisa * 4;
					}
					
				?>
				<?$i=0;foreach ($items->result() as $p):$i++;?>			
				<?
					$offstring = "";
					if(($offset > 0) && ($i == $highlight))
						$offstring = "offset-md-$offset";
				?>
				<div id="product-1-i" class="col-md-4 product <?=$offstring?>">
					<div class="product-block">
						<div class="product-bg">
							<?if ($data->type == 'type_2'): ?>
								<div style="background: url(<?=base_url()?>assets/section/<?=$p->image_bg;?>) center no-repeat !important;" class="product-bg-img"></div>
								<div class="product-bg-overlay"></div>
							<?endif;?>
						</div>
						<div class="product-content">
							<h3><?=$p->title;?></h3>
							<p <?=(($data->type == 'type_2') ? 'class="version-bg"' : '');?> ><?=$p->description;?></p>
							<?if ($p->price_dsc > 0): ?>
								<span class="sale">Rp <?=format_number($p->price);?></span><br>
								<span class="sale-price">Rp <?=format_number($p->price_dsc);?></span>
							<?else:?>
								<span>Rp <?=format_number($p->price);?></span>
							<?endif;?>
							<div class="product-img">
								<img src="<?=base_url()?>assets/section/<?=$p->image;?>" alt="<?=$p->title;?>">
							</div>
							<div class="bnt-block">
								<?php if ($p->available == "yes"): ?>
									<a href="#order" 
										data-id="<?=$p->id;?>" 
										data-image="<?=base_url()?>assets/section/<?=$p->image;?>" 
										data-title="<?=$p->title;?>" 
										data-description="<?=$p->description;?>" 
										data-description2="<?=$p->description2;?>" 
										data-price="<?=$p->price;?>" 
										data-price_dsc="<?=$p->price_dsc;?>" 
										data-toggle="modal"
										data-target=".pop-buy"
										data-whatever="@mdo"
										class="button buy-button" title="Buy Now">Buy Now</a>
								<?php else: ?>
									<a class="button buy-soldout" title="SOLD OUT">SOLD OUT</a>
								<?php endif ?>
								<br>
								<br>
							</div>
						</div>
						<div class="bottom_prd">
							<div class="view">
								<span>sedang dipesan : <?=rand($useeb,$useet)?></span>
								<span><i data-icn="icon-eye" class="icon-eye active"></i></span>
							</div>
							<div class="sold">
								<span style="margin-left: 20px"><i data-icn="icon-basket-loaded" class="icon-basket-loaded active"></i></span>
								<span>terjual : <?=$ubprda[$p->id]+$ubuy+$p->sold_display?></span>
							</div>
						</div>
					</div>
				</div>	
				<?endforeach;?>
		</div>
	</div>
</section>

<!-- pop up -->
<div class="modal fade pop-buy user-login" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header text-center">
				<div class="order-form-header clearfix">
					<div class="row">
						<div class="col-md-4">
							<div class="order-img">
								<img id="pop-image" src="" alt="product">
							</div>
						</div>
						<div class="col-md-8">
							<div class="order-desc">
								<b id="pop-title"></b>
								<span id="pop-description" class="version">Basic Green Version</span>
								<div id="pop-price"></div>
								<span id="pop-description2">Corporis suscipit dolorem, nisi, totam rem aperiam eaque ipsa.</span>
							</div>
						</div>
					</div>
				</div>
				<a class="close" data-dismiss="modal" aria-label="Close"><span  aria-hidden="true">&times;</span></a>
			</div>
			<div class="modal-body">
				<div class="user-form">
					<?=form_open('/',array("class"=>"order-form","id"=>"order-form"))?>
						<input type="hidden" name="customer_ip" value="<?=$this->customer_ip;?>">
						<ul class="nav nav-pills" id="pills-tab" role="tablist">
							<li class="nav-item">
								<a class="nav-link active show" id="login-tab" data-toggle="pill" role="tab" aria-controls="login" aria-selected="true">Alamat Pengiriman</a>
							</li>
						</ul>
						<div class="row">
							<input id="order-l-name" class="order-l-name" type="text" name="cust_l_name" placeholder="Last Name" style="display: none;" value="-">
							<input type="hidden" id="hd-prd_id" name="prd_id">
							<input type="hidden" id="hd-prd_coupon_code" name="prd_coupon_code">
							<input type="hidden" id="hd-prd_total" name="prd_total">
							<input type="hidden" id="hd-payment" name="payment" value="">
							<input type="hidden" id="hd-payment_detail" name="payment_detail" value="">
							<input type="hidden" id="hd-cust_kecamatan_id" name="cust_kecamatan_id" value="">
							<div class="col-md-12">
								<input id="order-f-name" class="form-control order-f-name" type="text" name="cust_f_name" placeholder="Nama Lengkap">
							</div>
							<div class="col-md-6">
								<input id="order-email" class="form-control order-email" type="text" name="cust_email" placeholder="Email">
							</div>
							<div class="col-md-6">
								<input id="order-phone" class="form-control order-phone" type="text" name="cust_phone" placeholder="Nomor Telepon">
							</div>
							<div class="col-md-12">
								<input id="order-address" class="form-control order-address" type="text" name="cust_address" placeholder="Alamat Lengkap">
							</div>
							<div class="col-md-6">
								<select id="order-province" class="form-control order-country" name="cust_province">
									<option value="0">Pilih Provinsi</option>
								</select>
							</div>
							<div class="col-md-6">
								<select id="order-city" class="form-control order-country" name="cust_city">
									<option value="0">Pilih Kota / Kab</option>
								</select>
							</div>
							<div class="col-md-6">
								<select id="order-kecamatan" class="form-control order-country" name="cust_kecamatan">
									<option value="0">Pilih Kecamatan</option>
								</select>
							</div>
							<div class="col-md-6">
								<input id="order-zip" class="form-control order-zip" type="text" name="cust_zip" placeholder="Kode Pos">
							</div>
							<div class="col-md-6">
								<span class="title-blue-pop">
									Jasa Pengiriman
								</span>
								<select id="order-courier" class="form-control order-country" name="cust_shipping">
									<option value="0">Pilih Jasa Pengiriman</option>
								</select>
							</div>
							<div class="col-md-6">
								<span class="title-blue-pop">
									Jumlah Barang
								</span>
								<input style="width: 80px;margin-top: 11px;display: inline;margin-top: 0px;margin-right: 10px;" id="order-quantity" class="form-control order-f-name" type="number" name="order_quantity" placeholder="qty" value="1" min="1"/>pcs
							</div>
							<div class="col-md-12 order-button-block">
								<button id="order-button" type="submit" class="button order-button" title="Order">Pesan Sekarang</button>
							</div>
						</div>
					</form>
				
					<!-- payment step -->
					<div id="content-payment">
						<div id="payment-step">
							<div class="col-md-12">
								<ul class="nav nav-pills" id="pills-tab" role="tablist">
									<li class="nav-item">
										<a class="nav-link active show" id="login-tab" data-toggle="pill" role="tab" aria-controls="login" aria-selected="true">Metode Pembayaran</a>
									</li>
								</ul>
							</div>
							<div class="row">
								<? $mstpayment = $this->db->get_where('mst_payment',array('status'=>'enable','subdomain'=>subdomain())); ?>
								<?php foreach ($mstpayment->result() as $m): ?>
									<div class="col-md-6">
										<?php if (strpos($m->code, "doku") !== false): ?>
											<div class="select_payment doku" data-value="<?=$m->code;?>" data-description="<?=$m->description;?>">
												<div class="row">
													<div class="col-md-3"><img src="https://nexbay.id/assets/images/doku.png" style="width:!00px"></div>	
													<div style="font-size:11px !important" class="col-md-9 "><?=$m->name;?></div>
												</div>
											</div>		
										<?php elseif (strpos($m->code, "kredivo") !== false): ?>

											<div class="select_payment doku" data-value="<?=$m->code;?>" data-description="<?=$m->description;?>">
												<div class="row">
													<div class="col-md-3"><img src="https://yt3.ggpht.com/a-/AN66SAytWq1KxaSUEAB120OqP3WmjYXfN5vu8RifCQ=s900-mo-c-c0xffffffff-rj-k-no" style="width:!00px"></div>	
													<div class="col-md-9 "><?=$m->name;?></div>
												</div>
											</div>

										<?php else: ?>
											<div class="select_payment" data-value="<?=$m->code;?>" data-description="<?=$m->description;?>">
												<?=$m->name;?>
											</div>
										<?php endif ?>
									</div>	
								<?php endforeach ?>
							</div>
							<div class="clearfix"></div>
							<div id="pay-sum" class="paysum">
								<div class="card text-white bg-warning">
									<div class="card-body">
										<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
									</div>
								</div>
							</div>
							<div id="pay-sum-bank" class="paysum">
								<div class="card text-white bg-warning">
									<div class="card-body">
										<p class="card-text below">Please select bank below</p>
										<? $rek = $this->db->get_where('mst_payment_transfer',array('subdomain'=>subdomain()));?>
										<select id="payment_transfer_option" class="order-country" name="order-country">
											<?foreach ($rek->result() as $d): ?>
											<option value="<?=$d->bank;?> : <?=$d->account_number;?> -- <?=$d->name_holder;?>"><?=$d->bank;?> : <?=$d->account_number;?> -- <?=$d->name_holder;?></option>
											<?endforeach;?>
										</select>
									</div>
								</div>
							</div><br>
							<div id="pay-coupon">
								<center><b><a id="pay-coupon-have" href="#">Punya Kode Voucher?</a></b></center>
								<div id="pay-coupon-form" class="newsletter-field-group">
									<input id="coupon-input" type="text" name="newsletter-email" placeholder="masukkan kode kupon" style="width: 65%;border-radius: 20px;font-size: 13px;margin-left: 35px;border: 1px solid #cecece;padding: 5px 15px;border-radius: 10px;">
									<button id="coupon-submit" type="submit" class="button submit-button" style="height: 37px;line-height: 14px;width: 25%;text-align: center;padding: 0px 0px;margin: 0px;margin-left: -40px;border-radius: 0px 10px 10px 0px;cursor: pointer;">check</button>
								</div>
								<center><b><a id="pay-coupon-valid" href="#"></a></b></center>
							</div>
							<div class="col-md-12 order-button-block">
								<button id="order-button-payment" type="submit" class="button order-button" title="Order">Lanjutkan</button>
							</div>
						</div>
					</div>
					<!-- end payment step -->
				
					<!-- confirm step -->
					<div id="content-confirmation">
						<div id="order-step2">
							<div class="col-md-12">
								<ul class="nav nav-pills" id="pills-tab" role="tablist">
									<li class="nav-item">
										<a class="nav-link active show" id="login-tab" data-toggle="pill" role="tab" aria-controls="login" aria-selected="true">Ringkasan Pemesanan</a>
									</li>
								</ul>
							</div>
							<div class="col-md-12">
								<table class="table table-striped">
									<tr>
										<td id="confirm-prd"></td>
										<td style="text-align:right" id="confirm-price">Rp 2.000.000</td>
										<td></td>
									</tr>
									<tr>
										<td>Jasa Pengiriman : <span style="text-transform: uppercase;font-weight: bold;" id="confirm-ship-type"></span></td>
										<td id="confirm-ship" style="text-align:right"></td>
										<td></td>
									</tr>
									<tr id="row-coupon" class="hide">
										<td id="confirm-coupon-code">Coupon Code : <b></b></td>
										<td id="confirm-coupon-code-value" style="text-align:right"></td>
										<td>(-)</td>
									</tr>
									<tr id="row-total">
										<td>Total</td>
										<td id="confirm-grand-total" style="text-align:right"></td>
										<td></td>
									</tr>
								</table>
							</div>
							<div class="col-md-12">
								<span class="title-blue-pop">Dikirim Ke</span>
							</div>
							<div class="col-md-12">
								<b id="deliver-name">Agus, Prayogo</b>
								<p id="deliver-address">Jalan kjlhd kalsjdsad asjdasd,</p>
								<p id="deliver-prov">ZipCode : 29832, Prov. Sulut, Kab. alsjdasd</p>
								<P id="deliver-email">Email : 23948324, HP : 238493204</P>
								<P id="deliver-shipping">Shipping Method : JNE</P>
								<P id="deliver-payment">Payment Method : Transfer Bank</P>
							</div>
							<div class="col-md-12 order-button-block">
								<button id="order-button-final" type="submit" class="button order-button" title="Order">Konfirmasi Pesanan</button>
							</div>
						</div>
					</div>
					<!-- end confirm step -->
				</div>	
			</div>
		</div>
	</div>
</div>