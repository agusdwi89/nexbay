<style type="text/css">
	#flexreview{
		background: transparent !important;
	}
	#flexreview .flex-next,#flexreview .flex-prev{
		font-family: 'simple-line-icons';
		speak: none;
		font-style: normal;
		font-weight: normal;
		font-variant: normal;
		text-transform: none;
		line-height: 1;
		/* Better Font Rendering =========== */
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
	}
	div.review{padding-left: 50px;}
	#flexreview .flex-prev:before{content: "\e605" !important;font-family: 'simple-line-icons' !important; color:#4cc3d3;}
	#flexreview .flex-next:before{content: "\e606" !important;font-family: 'simple-line-icons' !important; color:#4cc3d3;}
	ul.slides li{
		list-style: none;
	}
	
</style>

<?
	$items = $this->db->get_where('section_testimonial_items',array('testi_id'=>$data->id));	
?>
<section id="testimonial" class="reviews block gray">
	<div class="container">

		<div class="row">
			<div class="col-lg-6">
				<div class="title-box">
					<h2 class="title"><?=$data->title_first;?> <span class="hight-text"><?=$data->title_highlight;?></span> <?=$data->title_last;?></h2>
					<div class="v-text text-uppercase"><span><?=$data->number;?></span></div>
				</div>		
			</div>
			<div class="col-lg-6">
				<p><?=$data->description;?></p>
			</div>
		</div>

		<div class="row">
			<div id="flexreview" class="flexslider slider">
				<ul class="slides">
					
					<?$i=0;foreach ($items->result() as $r ): $i++;?>

						<?php if ($i % 2 == 1): ?>
							<li style="background: transparent;">		
								<div style="margin-right:0px !important" class="row">
						<?php endif ?>
							<div id="review-1" class="review col-md-6">
								<p><?=$r->review;?></p>
								<div class="author">
									<img src="<?=base_url()?>assets/section/<?=$r->photo;?>" alt="author">
									<h4><?=$r->name;?></h4>
								</div>
							</div>
						<?php if ($i % 2 == 0): ?>
							</div>
							</li>
						<?php endif ?>
					<?endforeach;?>
					<?php if ($i % 2 == 1): ?>
						</div>
						</li>
					<?php endif ?>
				</ul>
			</div>
			<br>
		</div>
	</div>
</section>