<style type="text/css">
	#clocksale{
		width: 460px;
		height: 116px;
		margin: 0 auto !important;
	}

</style>
<section id="buy" class="cta block color">
	<div class="cta-parallax-1"></div>
	<div class="cta-parallax-2"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2><?=$data->title;?></h2>
				<div id="clocksale" class="clock"></div>

				<?php if ($data->button_type == "section"): ?>
					<a id="btn_section_buy_<?=$data->id;?>" href="#<?=$data->button_link;?>" class="button action-button-cta smooth" title="<?=$data->button_text;?>"><?=$data->button_text;?></a>
				<?php else: ?>
					<a id="btn_section_buy_<?=$data->id;?>" href="<?=$data->button_link;?>" class="button action-button-cta" title="<?=$data->button_text;?>" target="_blank"><?=$data->button_text;?></a>
				<?php endif;?>
				
			</div>
			<div class="clearfix"></div><div class="clear"></div><br>
		</div>
	</div>
</section>
<?$this->button_script_id['section_buy'][] = $data->id;?>