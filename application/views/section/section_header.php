<?$items = $this->db->get_where('section_header_items',array('header_id'=>$data->id));?>

<div id="banner" class="banner" style="background-image: url('<?=base_url()?>assets/section/<?=$data->image_bg;?>'); background-position: center right; background-size: contain;">
	<div class="container-fluid">
		<div class="row no-gutters">
			<div class="col-lg-7 col-md-8 col-sm-12">
				<div class="banner-text">
					<img class="img-fluid wow fadeInRight" src="<?=base_url()?>assets/section/<?=$data->image;?>" alt="image" data-wow-duration="1s">
					<div class="img-bg">
					</div>
					<div class="row">
						<div class="col-lg-8 col-md-8 col-sm-6">
							<div class="owl-carousel" data-nav-dots="true" data-nav-arrow="false" data-items="1" data-xs-items="1" data-sm-items="1" data-md-items="1" data-lg-items="1" data-autoplay="true">

								<?foreach ($items->result() as $it): ?>
									<div class="item">
										<span class="text-uppercase"><?=$it->title_first;?></span>
										<h2 class="title text-uppercase"><?=$it->title_highlight;?></h2>
										<p><?=$it->title_last;?></p>

										<?if (strlen($it->button_text) > 0): ?>
											<?if ($it->button_type == "section"): ?>
												<a class="button" href="#<?=$it->button_link;?>"><?=$it->button_text;?></a>
											<?else:?>
												<a class="button" target="_blank" href="<?=$it->button_link;?>"><?=$it->button_text;?></a>
											<?endif;?>
										<?endif;?>
									</div>									
								<?endforeach;?>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12 text-right">
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-5 col-md-4 col-sm-12">
			</div>
		</div>
	</div>
</div>