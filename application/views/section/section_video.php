<section id="video" class="video jarallax" style="background-image: url('<?=base_url()?>assets/section/<?=$data->image;?>'); background-position: left bottom;">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 wow fadeInLeft" data-wow-duration="1s">
				<div class="title-box white">
					<h2 class="title"><?=$data->title;?></h2>
					<div class="v-text text-uppercase"><span><?=$data->number;?></span></div>
				</div>
				<p class="white">
					<?=$data->description;?>
				</p>
			</div>
			<div class="col-lg-7 wow fadeInRight" data-wow-duration="1s">
				<iframe id="vid-youtube"  src="https://www.youtube.com/embed/<?=$data->link;?>">
				</iframe>
			</div>
		</div>
	</div>
</section>