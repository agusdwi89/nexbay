<?$items = $this->db->get_where('section_features_items',array('sf_id'=>$data->id));?>

<section id="features" class="features">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="title-box">
					<h2 class="title"><?=$data->title_first;?> <span class="hight-text"><?=$data->title_highlight;?></span> <?=$data->title_last;?></h2>
					<div class="v-text text-uppercase"><span><?=$data->number;?></span></div>
				</div>
				<p class="big-text"><?=$data->subtitle;?></p>
				<p><?=$data->description;?></p>
			</div>
			<div class="col-lg-6">
				<ul class="features-box">
					<?foreach ($items->result() as $item): ?>
						<li class="text-center">
							<img class="img-fluid wow fadeInUp" data-wow-duration="1s" src="<?=base_url()?>assets/section/<?=$item->image;?>" alt="image">
							<div class="title wow fadeInUp" data-wow-duration="1.5s"><?=$item->title;?></div>
						</li>
					<?endforeach;?>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="features-content text-center">
					<img class="img-fluid" src="<?=base_url()?>assets/section/<?=$data->image;?>" alt="image">
				</div>
			</div>
		</div>
	</div>
</section>