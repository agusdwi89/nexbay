<section id="services" class="services">
	<div class="container-fluid">
		<div class="row no-gutters">

			<?if ($data->image_position == "left"): ?>
				<div class="col-lg-7 col-md-6 wow fadeInLeft" data-wow-duration="1s" style="background-image: url('<?=base_url()?>assets/section/<?=$data->image;?>'); background-position: center left;background-size: cover;"></div>
			<?endif;?>
			
			<div class="col-lg-5 col-md-6 wow fadeInRight" data-wow-duration="1s">
				<div class="title-box">
					<div class="v-text text-uppercase"><span><?=$data->number;?></span></div>
				</div>
				<div class="services-box">
					<h2><?=$data->title_first;?> <span class="hight-text"><?=$data->title_highlight;?></span> <?=$data->title_last;?></h2>
					<p><?=$data->description;?></p>

					<?if (strlen($data->button_text) > 0): ?>
						<br>
						<?if ($data->button_type == "section"): ?>
							<a class="button" href="#<?=$data->button_link;?>"><?=$data->button_text;?></a>
						<?else:?>
							<a class="button" target="_blank" href="<?=$data->button_link;?>"><?=$data->button_text;?></a>
						<?endif;?>
					<?endif;?>

				</div>
			</div>

			<?if ($data->image_position == "right"): ?>
				<div class="col-lg-7 col-md-6 wow fadeInLeft" data-wow-duration="1s" style="background-image: url('<?=base_url()?>assets/section/<?=$data->image;?>'); background-position: center left;background-size: cover;"></div>
			<?endif;?>

		</div>
	</div>
</section>

<?$this->button_script_id['section_about'][] = $data->id;?>