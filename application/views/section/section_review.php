<?$items = $this->db->get_where('section_review_items',array('srev_id'=>$data->id));?>
<section id="partners" class="partners">
	<div class="partners-box">
		<img class="img-fluid car-img wow fadeInLeft" data-wow-duration="1s" src="<?=base_url()?>assets/section/<?=$data->image;?>" alt="image">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 bg-1">
					<h4><?=$data->title_left;?></h4>
					<p><?=$data->description_left;?></p>
				</div>
				<div class="col-sm-6 bg-2">
					<h4><?=$data->title_right;?></h4>
					<p><?=$data->description_right;?></p>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="partners-content text-center">
					<img class="img-fluid wow flipInX" data-wow-duration="1s" src="<?=base_url()?>assets/section/<?=$data->image_logo;?>" alt="logo">
					<h2 class="wow fadeInUp" data-wow-duration="1s"><?=$data->title_bottom;?> <span class="hight-text"><?=$data->title_bottom_highlight;?></span></h2>
					<p class="wow fadeInUp" data-wow-duration="1s"><?=$data->description;?></p>

					<?if (strlen($data->button_text) > 0): ?>
						<?if ($data->button_type == "section"): ?>
							<a class="button wow fadeInUp" role="button" href="<?=base_url()?>#<?=$data->button_link;?>"><?=$data->button_text;?></a>
						<?else:?>
							<a class="button wow fadeInUp" role="button" target="_blank" href="<?=$data->button_link;?>"><?=$data->button_text;?></a>
						<?endif;?>
					<?endif;?>

				</div>
			</div>
		</div>
	</div>
	<!-- <div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<div id="animated_div"></div>
			</div>
		</div>
	</div> -->
</section>