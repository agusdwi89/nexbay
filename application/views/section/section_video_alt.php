<section id="video_alt" class="about">
	<div class="container-fluid">
		<div class="row no-gutters">
			<div class="col-lg-7 col-md-4 col-sm-12" style="background-image: url('<?=base_url()?>assets/section/<?=$data->image_bg;?>'); background-position: center right;">
			</div>
			<div class="col-lg-5 col-md-8 col-sm-12">
				<div class="about-text">
					<div class="video-box wow fadeInLeft" data-wow-duration="1s">
						<div class="video-play" style="background-image: url('<?=base_url()?>assets/section/<?=$data->image_bg_play;?>'); background-position: center center;">
							<a href="https://www.youtube.com/watch?v=<?=$data->link;?>" class="popup-youtube"> 
								<img class="img-fluid" src="<?=base_url()?>assets/geniot/images/play.png" alt="image"> 
							</a>
						</div>
					</div>
					
					<div class="title-box">
						<h2 class="title"><?=$data->title_first;?> <span class="hight-text"><?=$data->title_highlight;?></span> <?=$data->title_last;?></h2>
						<div class="v-text text-uppercase"><span><?=$data->number;?></span></div>
					</div>
					
					<div class="silly_scrollbar">
						<?=$data->description;?>
					</div>

					<?if (strlen($data->button_text) > 0): ?>
						<?if ($data->button_type == "section"): ?>
							<a class="read-more" href="<?=base_url()?>#<?=$data->button_link;?>"><?=$data->button_text;?></a>
						<?else:?>
							<a class="read-more" target="_blank" href="<?=$data->button_link;?>"><?=$data->button_text;?></a>
						<?endif;?>
					<?endif;?>

				</div>
			</div>
		</div>
	</div>
</section>