<?
	$socials = $this->db->get_where('section_footer_social',array('sfoot_id'=>$data->id))->row();
	$links = $this->db->get_where('section_footer_links',array('sfoot_id'=>$data->id));	
?>

<footer id="footer" class="footer">
	<div class="footer-middel">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-12 col-sm-12 pad features map-tooltip">
					<div class="text-center">
						<a class="logo-img" href="#">
							<img class="img-fluid" src="<?=base_url()?>assets/geniot/images/logo.png" alt="logo">
						</a>
						<div class="map">
							<img class="img-fluid" src="<?=base_url()?>assets/geniot/images/map.png" alt="logo">
						</div>
					</div>
					<a href="https://goo.gl/maps/wfLh9aMNwPo">
						<span class="tooltip one">
							<span class="tooltip-item"></span>
						</span>
					</a>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6">
							<?$dbtpm = $this->db->get_where('top_menu',array('subdomain'=>subdomain()));?>
							<h5 class="title">Informasi</h5>
							<ul class="footer-link">
								<?php $i=0; foreach ($dbtpm->result() as $v): $i++;?>
									<li><a href="#<?=$v->url;?>" title="<?=$v->name;?>"><?=$v->name;?></a></li>	
								<?php endforeach ?>
							</ul>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 re-575">
							<h5 class="title">Produk Lainnya</h5>
							<?$op=$this->db->get('other_products');?>
							<ul class="footer-link">
								<?php foreach ($op->result() as $o): ?>
									<li><a target="_blank" href="<?=$o->url;?>"><?=$o->name;?></a></li>	
								<?php endforeach ?>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-5 col-md-6 col-sm-12 re-767">
					<h5 class="title">Hubungi Kami</h5>
					<div class="contact">
						<ul>
							<li>
								<span>Whatsapp:</span> 
								<a href="https://api.whatsapp.com/send?phone=<?=$this->db->get_where('global_config',array('name'=>'phone'))->row()->value;?>&text=Halo%20Admin%20Saya%20Mau%20Order%20Produk%20Dari%20Nexbay%20Store">
									<?=$this->db->get_where('global_config',array('name'=>'phone'))->row()->value;?>
								</a> 
							</li>
							<li><span>Mail:</span> <?=$this->db->get_where('global_config',array('name'=>'mail'))->row()->value;?></li>
						</ul>
						<div class="footer-copyright">
							<?=$this->db->get_where('global_config',array('name'=>'copyright'))->row()->value;?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>