<?$items = $this->db->get_where('section_services_items',array('sf_id'=>$data->id));?>

<section id="services" class="smart-features">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="title-box">
					<h3 class="title"><?=$data->title_first;?> <span class="hight-text"><?=$data->title_highlight;?></span><?=$data->title_last;?></h3>
					<div class="v-text text-uppercase"><span>07</span></div>
				</div>
			</div>
			<div class="col-lg-6">
				<p><?=$data->description;?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<ul>
					<?foreach ($items->result() as $item): ?>
						<li>
							<div class="smart-box wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
								<div class="left">
									<img class="img-fluid" src="<?=base_url()?>assets/section/<?=$item->image;?>" alt="image">
								</div>
								<div class="right">
									<h5><?=$item->title;?></h5>
									<p><?=$item->description;?></p>
								</div>
							</div>
						</li>	
					<?endforeach;?>
				</ul>
			</div>
		</div>
	</div>
</section>