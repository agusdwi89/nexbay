<section id="timer" class="reviews block gray" style="background:url('<?=base_url()?>assets/section/<?=$data->image;?>');padding:50px 0px">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="partners-content text-center" style="margin:0px">
					<h2 style="color:white" class="wow fadeInUp" data-wow-duration="1s"><?=$data->title;?></h2>
					<p style="color:white" class="wow fadeInUp" data-wow-duration="1s"><?=$data->description;?></p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div id="page">
					<div class="container-fluid">
						<div class="row">
							<div id="info" class="col-md-12 text-white text-center page-info col-transform">
								<div class="vert-middle">
									<div class="reveal scale-out">
										<?
											$stop_date = new DateTime(  DATE('Y-m-d h:i:s'));
											$stop_date->modify('+'.($data->day+13).' hours');
										?>
										<div class="count-down p-t-b-15" data-end-date="<?=$stop_date->format('M d, Y H:i:s');?>">
										</div>
									</div>
								</div>
							</div>
							<div class="col-12" style="text-align:center">
								<a class="button" href="#product">BELI SEKARANG</a>
							</div>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</div>
</section>