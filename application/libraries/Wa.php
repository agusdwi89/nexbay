<?
class Wa {

	private $CI;

	function __construct(){
		$this->CI =& get_instance();
		$this->API_URL 		= "https://api.wassenger.com/v1/messages";
		$this->API_TOKEN 	= "29f87af75ded17351432cc02182386cfb2a775eca4f12be242593fb79e06d0da5bdc96691ca6a52d";
	}

	function format_phone_number($phone){
		$num = $phone;
		$phone = str_replace(" ","",$phone);
		$phone = "##$phone";

		if (stripos($phone, "##08") === 0) {
			$num = str_replace("##08", "+628", $phone);
		}else if(stripos($phone, "##628") === 0){
			$num = str_replace("##628", "+628", $phone);
		}else if(stripos($phone, "##+628") === 0){
			$num = str_replace("##+628", "+628", $phone);
		}
		return $num;
	}

	function parse_order($id){
		$data 			= $this->CI->db->get_where('v_manage_order',array('id'=>$id))->row();
		$this->phone 	= $this->format_phone_number($data->cust_phone);
		$this->order 	= "NEX1".sprintf("%'.05d", $data->id);
		$this->link 	= base_url()."home/order/".$data->link_unique;
		$this->title 	= trim($data->prd_title);
		$this->name 	= trim($data->cust_f_name);
		$this->price 	= format_number($data->prd_total);
		$this->resi 	= $data->resi;
		$this->pay_method = $data->payment;
	}

	function send($no, $msg){
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->API_URL,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => '{"phone":"'.$no.'","message":"'.$msg.'"}',
			CURLOPT_HTTPHEADER => array(
				"content-type: application/json",
				"token: {$this->API_TOKEN}"
				),
			));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		// echo "cURL Error #:" . $err;
		} else {
		// echo $response;
		}
	}

	function notif_checkout($id){
		$this->parse_order($id);
		if ($this->pay_method == "cod")
			$this->checkout_cod();
		else $this->checkout_transfer();
	}

	// notif COD

	function checkout_cod(){
		$txt  = "Hi *{$this->name}*, \\n\\n";
		$txt .= "Terimakasih telah berbelanja di *NEXBAY.ID*. \\n\\n";
		$txt .= "Pesanan anda *{$this->title}* telah kami terima dengan no order *{$this->order}* nominal *Rp.{$this->price}* pembayaran melalui *COD (Bayar Ditempat)*. \\n\\n";
		$txt .= "*Konfirmasikan Pemesanan* Anda dengan membalas pesan ini agar dapat dikirimkan dengan membalas *'Konfirmasi'*. \\n\\n";
		$txt .= "Untuk memantau status order. Silahkan klik link dibawah ini : \\n\\n";
		$txt .= "{$this->link} \\n\\n";
		$txt .= "*NEXBAY.ID*";

		$this->send($this->phone,$txt);
	}	

	function cod_confirm($id){
		$this->parse_order($id);

		$txt  = "Hi *{$this->name}*, \\n\\n";
		$txt .= "*Pesanan Terkonfirmasi* untuk produk *{$this->title}* telah kami terima dengan nomor order *{$this->order}*. \\n\\n";
		$txt .= "Pembayaran nominal *Rp.{$this->price}* dapat dilakukan ketika pesanan sudah sampai dirumah *(COD)*. \\n\\n";
		$txt .= "Untuk memantau status order. Silahkan klik link dibawah ini : \\n\\n";
		$txt .= "{$this->link} \\n\\n";
		$txt .= "*NEXBAY.ID*";

		$this->send($this->phone,$txt);
	}

	// notif transfer

	function checkout_transfer(){
		$txt  = "Hi *{$this->name}*, \\n\\n";
		$txt .= "Terimakasih telah berbelanja di *NEXBAY.ID*. \\n\\n";
		$txt .= "Pesanan anda *{$this->title}* telah kami terima dengan no order *{$this->order}* nominal *Rp.{$this->price}* pembayaran melalui *Transfer*. \\n\\n";
		$txt .= "Konfirmasikan Pembayaran Anda dengan membalas pesan ini beserta *Bukti Transfer* atau bisa diupload melalui link dibawah ini : \\n\\n";
		$txt .= "{$this->link} \\n\\n";
		$txt .= "*NEXBAY.ID*";

		$this->send($this->phone,$txt);
	}	

	function transfer_succeed($id){
		$this->parse_order($id);

		$txt  = "Hi *{$this->name}*, \\n\\n";
		$txt .= "Pembayaran Sukses untuk pesanan *{$this->title}* dengan nomor order *{$this->order}*. \\n\\n";
		$txt .= "Untuk memantau status order. Silahkan klik link dibawah ini : \\n\\n";
		$txt .= "{$this->link} \\n\\n";
		$txt .= "*NEXBAY.ID*";

		$this->send($this->phone,$txt);
	}	

	// general

	function notif_delivery($id){
		$this->parse_order($id);

		$txt  = "Hi *{$this->name}*, \\n\\n";
		$txt .= "*Update Resi Pengiriman* untuk produk *{$this->title}* dengan nomor order *{$this->order}* yaitu *{$this->resi}*. \\n\\n";
		$txt .= "Untuk memantau status order. Silahkan klik link dibawah ini : \\n\\n";
		$txt .= "{$this->link} \\n\\n";
		$txt .= "*NEXBAY.ID*";

		$this->send($this->phone,$txt);
	}

	function notif_order_finish($id){
		$this->parse_order($id);

		$txt  = "Hi *{$this->name}*, \\n\\n";
		$txt .= "Pesanan anda berupa *{$this->title}* dengan nomor order *{$this->order}* berhasil diterima. \\n\\n";
		$txt .= "Terima kasih telah berbelanja di *NEXBAY.ID*. \\n\\n";

		// order finish notif off temporary
		// $this->send($this->phone,$txt);	
	}
}