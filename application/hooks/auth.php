<?
	class Auth{
		var $CI;

		function __construct(){
			$this->CI =& get_instance();
		}
		
		function cek(){

			$uri1 = $this->CI->uri->segment(1);
			$uri2 = $this->CI->uri->segment(2);

			// reject manage admin from subdomain
			if ($uri1 == 'manage'){
				if (substr_count($_SERVER['HTTP_HOST'],'.') > 1){
					$loginurlplain = substr($_SERVER['HTTP_HOST'], strpos($_SERVER['HTTP_HOST'], '.') + 1)."/manage/login";	
					redirect("http://".$loginurlplain);
				}
			}

			if ($uri1 == 'manage' && $uri2 != 'login') {

				if(!is_login()){
					$this->CI->session->set_flashdata('message',"Maaf session anda sudah habis, silahkan login");
					redirect(base_url('manage/login'));
				}else{
					//cek permission and reject unauth requests
				}
			}
		}
	}
?>