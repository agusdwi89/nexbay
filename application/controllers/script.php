<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Script extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->customer_ip 	= $this->input->ip_address();
	}

	function lastbuy(){
		$this->load->view('script/v_lastbuy');
	}
}