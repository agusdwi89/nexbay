<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron extends CI_Controller {

	function __construct(){
		parent::__construct();
		// if ($_SERVER['REMOTE_ADDR'] != '116.203.42.16') show_404();
	}

	function wa(){
		$this->load->library("wa");
		$data = $this->db->limit(5)->get_where('cron_wa',array('status'=>'pending'));

		foreach ($data->result() as $w) {
			if ($w->type_wa == "notif_delivery") {
				$this->email_update($w->order_id,"resi");

				$this->wa->notif_delivery($w->order_id,'resi');
				
				$this->db->where('id', $w->id);
				$this->db->update('cron_wa', array('status' => 'delivered')); 
			}
		}

		$this->db->insert('log_cron',array('num'=> $data->num_rows()));
	}

	function email_update($id,$state){

		$data['db'] = $this->db->get_where('v_manage_order',array('id'=>$id))->row();
		$email = $data['db']->cust_email;
		$data['email_state'] = $state;

		$subject = "";

		switch ($state) {
			case 'payment-success':
				$subject = "Pembayaran Sukses - ".$data['db']->prd_title;
				break;
			case 'order-confirmed':
				$subject = "Pesanan Terkonfirmasi - ".$data['db']->prd_title;
				break;
			case 'delivery-process':
				$subject = "Pesanan Dalam Pengiriman - ".$data['db']->prd_title;
				break;
			case 'resi':
				$subject = "Update Resi Pengiriman - ".$data['db']->prd_title;
				break;
			default:
				$subject = "Pesanan Diterima - ".$data['db']->prd_title;
				break;
		}

		$d = $this->load->view('v_email',$data,true);
		// test email
		// $this->load->view('v_email',$data);
		// return true;

		$config = Array(
			'protocol' 	=> 'smtp',
			'smtp_host' => 'ssl://smtp.zoho.com',
			'smtp_port' => 465,
			'smtp_user' => 'info@nexbay.id',
			'smtp_pass' => 'Svr241090!',
			'mailtype'  => 'html', 
			'charset'   => 'iso-8859-1'
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$this->email->from('info@nexbay.id', 'NexBay Indonesia');
		$this->email->to($email); 

		$this->email->subject($subject);
		$this->email->message($d);  

		$this->email->send();
	}
}	
