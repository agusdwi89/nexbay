<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Section_review extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "section";
		$this->sub_domain = $this->session->userdata('session_subdomain');
	}
	
	function edit($id){
		if (is_post()) {
			$item = $this->input->post();

			$btn = $this->input->post('ar');
			$link = $this->input->post('state');
			$btn['button_link']	= $link[$btn['button_type']];

			if ($_FILES['image']['size'] > 0 ){
				$upload = $this->upload('image');
				if ($upload[0])
					$item['image'] = $upload[1]['file_name'];
			}

			if ($_FILES['image_logo']['size'] > 0 ){
				$upload = $this->upload('image_logo');
				if ($upload[0])
					$item['image_logo'] = $upload[1]['file_name'];
			}
			unset($item['ar']);unset($item['state']);
			$item = array_merge($item,$btn);

			$this->db->where('id', $id);
			$this->db->update('section_review', $item); 

			$this->session->set_flashdata('message','Data Saved Successfully');
			redirect(base_url("manage/section_review/edit/$id"));
		}

		$data['id'] 		= $id;
		$data['master'] 	= $this->db->get_where('section_review',array('id'=>$id))->row();
		$data['items'] 		= $this->db->get_where('section_review_items',array('srev_id'=>$id));
		$data['sectionopt'] = $this->db->get_where('v_section_name',array('subdomain'=>$this->sub_domain));
		$data['local_view'] = 'v_section_review';
		$this->load->view('v_manage',$data);
	}

	function upload($name){
		$_FILES['userfile']['name']	= strtolower($_FILES['userfile']['name']);
		$config['upload_path']		= 'assets/section';
		$config['allowed_types']	= 'jpg|png';
		$config['max_size']			= '10000';
		$config['max_width']		= '5000';
		$config['max_height']		= '5000';
		$config['encrypt_name']		= true;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload($name)){
			return array(false,$this->upload->display_errors());
		}else{
			$a = $this->upload->data();
			return array(true,$a);
		}
	}

	function add_customer($id){
		if ($_FILES['userfile']['size'] > 0 ){
			$upload = $this->upload('userfile');
			if ($upload[0]){
				$item['image'] 		= $upload[1]['file_name'];
				$item['srev_id']	= $id;
				$this->db->insert('section_review_items',$item);
				$this->session->set_flashdata('message','Data Saved Successfully');
				redirect(base_url("manage/section_review/edit/$id"));			
			}else{
				$this->session->set_flashdata('message','Upload Failed');
				redirect(base_url("manage/section_review/edit/$id"));			
			}
		}
	}		

	function delete_partners($id,$ids){
		$this->db->delete('section_review_items', array('id' => $id)); 
		$this->session->set_flashdata('message','Data deleted Successfully');
		redirect(base_url("manage/section_review/edit/$ids"));			
	}
}