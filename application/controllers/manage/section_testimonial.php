<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Section_testimonial extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "section";
		$this->sub_domain = $this->session->userdata('session_subdomain');
	}
	
	function edit($id){
		if (is_post()) {
			$item = $this->input->post();

			$this->db->where('id', $id);
			$this->db->update('section_testimonial', $item); 

			$this->session->set_flashdata('message','Data Saved Successfully');
			redirect(base_url("manage/section_testimonial/edit/$id"));
		}

		$data['id'] 		= $id;
		$data['master'] 	= $this->db->get_where('section_testimonial',array('id'=>$id))->row();
		$data['items'] 		= $this->db->get_where('section_testimonial_items',array('testi_id'=>$id));
		$data['local_view'] = 'v_section_testimonial';
		$this->load->view('v_manage',$data);
	}

	function upload($name){
		$_FILES['userfile']['name']	= strtolower($_FILES['userfile']['name']);
		$config['upload_path']		= 'assets/section';
		$config['allowed_types']	= 'jpg|png';
		$config['max_size']			= '10000';
		$config['max_width']		= '5000';
		$config['max_height']		= '5000';
		$config['encrypt_name']		= true;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload($name)){
			return array(false,$this->upload->display_errors());
		}else{
			$a = $this->upload->data();
			return array(true,$a);
		}
	}

	function add_testi($id){
		if ($_FILES['userfile']['size'] > 0 ){
			$upload = $this->upload('userfile');
			if ($upload[0]){
				$item = $this->input->post();
				$item['photo'] 		= $upload[1]['file_name'];
				$item['testi_id']	= $id;
				$this->db->insert('section_testimonial_items',$item);
				$this->session->set_flashdata('message','Data Saved Successfully');
				redirect(base_url("manage/section_testimonial/edit/$id"));			
			}else{
				$this->session->set_flashdata('message','Upload Failed');
				redirect(base_url("manage/section_testimonial/edit/$id"));			
			}
		}else{
			$this->session->set_flashdata('message','Upload Failed');
			redirect(base_url("manage/section_testimonial/edit/$id"));			
		}
	}		

	function delete_testi($id,$ids){
		$this->db->delete('section_testimonial_items', array('id' => $id)); 
		$this->session->set_flashdata('message','Data deleted Successfully');
		redirect(base_url("manage/section_testimonial/edit/$ids"));			
	}
}