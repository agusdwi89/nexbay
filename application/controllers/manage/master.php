<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->sub_domain = $this->session->userdata('session_subdomain');
	}

	public function index()
	{
		$data['local_view'] = 'v_dashboard';
		$this->load->view('v_manage',$data);
	}

	function shipping(){

		$this->current_menu = "shipping";

		$data['ship']			= $this->db->get('mst_delivery');
		$data['map_city'] 		= $this->map_city();
		$data['map_province'] 	= $this->map_province();
		$data['local_view'] 	= 'master/v_shipping';
		$this->load->view('v_manage',$data);
	}

	function shipping_import(){
		$this->current_menu = "shipping";

		if (is_post()) {
			$this->upload();
		}else{
			$data['local_view'] 	= 'master/v_shipping_import';
			$this->load->view('v_manage',$data);	
		}
	}

	function shipping_csv(){
		$this->current_menu = "shipping";

		$ship			= $this->db->get('mst_delivery');
		$map_city		= $this->map_city();
		$map_province	= $this->map_province();
		$dd[] 			= array("No","Province","City","id_City","Kecamatan","id_kecamatan","jne","jnt","ninja","wahana","pos");

		$i=0;foreach ($ship->result() as $v) {$i++;
			$dd[] = array($i,$map_province[$map_city[$v->id_city]['province']],$map_city[$v->id_city]['city'],$v->id_city,$v->nama_kecamatan,$v->id_kecamatan,$v->jne,$v->jnt,$v->ninja,$v->wahana,$v->pos);
		}
		$this->download($dd);
	}

	function map_city(){
		$c = $this->db->get('mst_city');
		$cc = [];
		foreach ($c->result() as $k) {
			$cc[$k->id]['city']	 	= $k->city;
			$cc[$k->id]['province'] = $k->id_province;
		}
		// debug_array($cc);
		return $cc;
	}

	function map_province(){
		$c = $this->db->get('mst_province');
		$cc = [];
		foreach ($c->result() as $k) {
			$cc[$k->id]	 	= $k->province;
		}
		return $cc;
	}

	private function download($data){
		$date = date_create();
		$d = date_timestamp_get($date);
		$list = $data;
		$fp = fopen("assets/csv/$d.csv", 'w');
		foreach ($list as $fields) {
			fputcsv($fp, $fields);
		}
		fclose($fp);

		$file_name = "$d.csv";
		$file_url = base_url()."assets/csv/$d.csv";
		header('Content-Type: application/octet-stream');
		header("Content-Transfer-Encoding: Binary"); 
		header("Content-disposition: attachment; filename=\"".$file_name."\""); 
		readfile($file_url);
		exit;
	}

	function upload(){
		$_FILES['userfile']['name']	= strtolower($_FILES['userfile']['name']);
		$config['upload_path']		= 'assets/csv';
		$config['allowed_types']	= '*';
		$config['max_size']			= '100000';
		$config['encrypt_name']		= true;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload()){
			$this->session->set_flashdata('message',$this->upload->display_errors());
			redirect(base_url('manage/master/shipping_import'));
		}else{
			$a = $this->upload->data();
			$csv = $this->readCSV("assets/csv/".$a['file_name']);
			$this->writeToSQL($csv);
			$this->session->set_flashdata('message',"Data imported successfully");
			redirect(base_url('manage/master/shipping'));
		}
	}

	function readCSV($csvFile){
		$file_handle = fopen($csvFile, 'r');
		while (!feof($file_handle) ) {
			$line_of_text[] = fgetcsv($file_handle, 1024);
		}
		fclose($file_handle);
		return $line_of_text;
	}

	function writeToSQL($ar){
		unset($ar[0]);
		unset($ar[count($ar)]);

		foreach ($ar as $d) {
			$data []=array(
				"id_city" 			=> $d[3],
				"id_kecamatan" 		=> $d[5],
				"nama_kecamatan" 	=> $d[4],
				"jne" 				=> $d[6],
				"jnt" 				=> $d[7],
				"ninja" 			=> $d[8],
				"wahana" 			=> $d[9],
				"pos" 				=> $d[10]
				);
		}
		$this->db->truncate('mst_delivery');
		$this->db->insert_batch('mst_delivery', $data); 
	}

	function payment($id=0){
		$this->current_menu = "payment";
		if (is_post()) {
			$inp = $this->input->post();
			$data = array('description' =>  $inp['description']);
			$this->db->where('id', $inp['id']);
			$this->db->update('mst_payment', $data); 
			$this->session->set_flashdata('message','Data saved successfully');
			redirect(base_url('manage/master/payment/'.$inp['id']));	
		} 

		$data['id']				= $id;
		$data['payment']		= $this->db->get_where('mst_payment',array('subdomain'=> $this->sub_domain));
		$data['transfer']		= $this->db->get_where('mst_payment_transfer',array('subdomain'=>$this->sub_domain));
		$data['local_view'] 	= 'master/v_payment';
		$this->load->view('v_manage',$data);		
	}

	function payment_status(){
		if (is_post()) {
			$input = $this->input->post();
			$data = array('status' =>  $input['status']);
			$this->db->where('id', $input['id']);
			$this->db->update('mst_payment', $data); 
		}
	}

	function payment_transfer(){
		if (is_post()) {
			$input = $this->input->post();
			$input['subdomain'] = $this->sub_domain;
			$this->db->insert('mst_payment_transfer',$input);
			$this->session->set_flashdata('message','Data saved successfully');
			redirect(base_url('manage/master/payment/2'));
		}
	}

	function payment_transfer_delete($id){
		$this->db->delete('mst_payment_transfer', array('id' => $id)); 
		$this->session->set_flashdata('message','Data deleted successfully');
		redirect(base_url('manage/master/payment/2'));
	}

}