/*

Template: Geniot: Smart Product Landing Page
Author: iqonicthemes.in
Version: 1.0

*/

/*----------------------------------------------
Index Of Script
------------------------------------------------

1.Page Loader
2.Back To Top
3.Hide Menu
4.Header
5.Magnific Popup
6.Countdown
7.Wow Animation
8.Owl Carousel



------------------------------------------------
Index Of Script
----------------------------------------------*/

$(document).ready(function() {

    /*------------------------
    Page Loader
    --------------------------*/
    jQuery("#load").fadeOut();
    jQuery("#loading").delay(0).fadeOut("slow");



    /*------------------------
    Back To Top
    --------------------------*/
    $('#back-to-top').fadeOut();
    $(window).on("scroll", function() {
        if ($(this).scrollTop() > 250) {
            $('#back-to-top').fadeIn(1400);
        } else {
            $('#back-to-top').fadeOut(400);
        }
    });
    // scroll body to 0px on click
    $('#top').on('click', function() {
        $('top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });



    /*------------------------
    Hide Menu
    --------------------------*/
    $(".navbar a").on("click", function(event) {
        $(".navbar-collapse").collapse('hide');
    });



    /*------------------------
    Header
    --------------------------*/
    $('.side-link li a').on('click', function(e) {
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top - 0
        }, 1500);
        e.preventDefault();
    });
    $(window).on('scroll', function() {
        if ($(this).scrollTop() > 100) {
            $('header').addClass('menu-sticky');
        } else {
            $('header').removeClass('menu-sticky');
        }
    });



    /*------------------------
    Magnific Popup
    --------------------------*/
    $('.popup-gallery').magnificPopup({
        delegate: 'a.popup-img',
        tLoading: 'Loading image #%curr%...',
        type: 'image',
        mainClass: 'mfp-img-mobile',
        gallery: {
            navigateByImgClick: true,
            enabled: true,
            preload: [0, 1]
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
        }
    });

    $('.popup-youtube, .popup-vimeo, .iq-video, .popup-gmaps').magnificPopup({
        type: 'iframe',
        disableOn: 700,
        mainClass: 'mfp-fade',
        preloader: false,
        removalDelay: 160,
        fixedContentPos: false
    });



    /*------------------------
    Countdown
    --------------------------*/
    $('#countdown').countdown({
        date: '10/01/2019 23:59:59',
        day: 'Day',
        days: 'Days'
    });



    /*------------------------
    Wow Animation
    --------------------------*/
    new WOW().init();


    /*------------------------
    Owl Carousel
    --------------------------*/
    $('.owl-carousel').each(function() {
        var $carousel = $(this);
        $carousel.owlCarousel({
            items: $carousel.data("items"),
            loop: $carousel.data("loop"),
            margin: $carousel.data("margin"),
            nav: $carousel.data("nav"),
            dots: $carousel.data("dots"),
            autoplay: $carousel.data("autoplay"),
            autoplayTimeout: $carousel.data("autoplay-timeout"),
            navText: ['<i class="lnr lnr-chevron-left"></i>', '<i class="lnr lnr-chevron-right"></i>'],
            responsiveClass: true,
            responsive: {
                0: {
                    items: $carousel.data("items-mobile-sm")
                },
                490: {
                    items: $carousel.data("items-mobile")
                },
                992: {
                    items: $carousel.data("items-tab")
                },
                1190: {
                    items: $carousel.data("items-laptop")
                },
                1199: {
                    items: $carousel.data("items")
                }
            }
        });
    });


});

// start order
var city = [];
var citySelected;
var kurir = [];
var map_kec_kurir = [];

var real_prc = real_prc_dsc = 0;

$(function(){
    $.getJSON( BASE+"djson/delivery", function( data ) {
        var items = "";
        $.each( data['mst_province'], function( key, val ) {
            items += '<option value='+key+'>'+val+'</option>'
        });
        $("#order-province").append(items);

        city = data['mst_city'];
        kurir = data['mst_kurir'];
    });

    $( "#order-province" ).change(function () {
        citySelected = city[$("#order-province").val()];
        var items="<option value='0'>Pilih Kota / Kab</option>";
        if (citySelected!=0) {
            $.each( citySelected, function( key, val ) {
                items += '<option value='+key+'>'+val+'</option>'
            });
        }
        $("#order-city").html(items);
        $("#order-courier").html("<option value='0'>Pilih Jasa Pengiriman</option>");
        $("#order-kecamatan").html("<option value='0'>Pilih Kecamatan</option>");
    });

    $( "#order-city" ).change(function () {
        cityID = $(this).val();
        kurirSelected = kurir[cityID];
        var csrf                = $("#global-form").find("input");
        var usercode            = $(this).parent().find('input');
        var opt                 = {};
        opt[csrf.attr('name')]  = csrf.val();
        opt['city_id']          = cityID;
        $('body').css('cursor', 'wait');

        if (typeof map_kec_kurir[cityID] == 'undefined') {
            $.post(BASE+"home/get_kec",  opt,function(data){
                $('body').css('cursor', 'default');
                var li = [];
                var id_city = 0;
                $.each(data, function( key, val ) {
                    li.push(val);
                    id_city = val.id_city;
                });
                map_kec_kurir[id_city] = li;

                generate_kec();
            },"json");
        }else{
            generate_kec();
        }

        $("#order-courier").html("<option value='0'>Pilih Jasa Pengiriman</option>");
    })

    $( "#order-kecamatan" ).change(function () {
        var idx = $(this).find('option:selected').data('index_kec');
        var kurs = map_kec_kurir[cityID][idx];
        $("#hd-cust_kecamatan_id").val($(this).find('option:selected').data('id-kecamatan'));
        var items="<option value='0'>Pilih Jasa Pengiriman</option>";
        $.each(kurs.kurir, function( key, val ) {
            items += '<option data-kurir="'+val.name+'" data-kurir-price="'+val.price+'" value="'+val.name+'">'+val.name.toUpperCase()+' : Rp. '+formatNumber(val.price)+'</option>'
        });
        $("#order-courier").html(items);
    })

    $('body').on('click','.buy-button',function (e) {
        e.preventDefault();
            //reset state
        $("#content-payment").hide();
        $("#content-confirmation").hide();
        $("#order-form").show();
        $("#order-form").trigger('reset');
        $(".order-field-error").removeClass('order-field-error');
        $(".select_payment.active").removeClass("active");
        $("#pay-sum-bank").hide();
        $("#pay-sum").hide();
        $("#pay-coupon-form").hide();
        $("#pay-coupon-valid").hide();
        $("#pay-coupon-have").show();
        $("#coupon-input").val('');
        payValue = "";

        initHiddenForm($(this));

        $("#pop-image").attr('src',$(this).data('image'));
        $("#pop-title").html($(this).data('title'));
        $("#pop-description").html($(this).data('description'));
        $("#pop-price").html(setPrice($(this)));
        $("#pop-description2").html($(this).data('description2'));

        fbq('track', 'CompleteRegistration');
    });
})

function generate_kec(){
    var items="<option value='0'>Pilih Kecamatan</option>";
    $.each(map_kec_kurir[cityID], function( key, val ) {
        items += '<option data-index_kec="'+key+'" value="'+val.nama_kecamatan+'" data-id-kecamatan="'+val.id_kecamatan+'">'+val.nama_kecamatan+'</option>';
    });
    $("#order-kecamatan").html(items);
}

function formatNumber(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function initOrder() {
  var order_form = $('#order-form');
  var order_button = $('#order-button');
  var order_f_name = $('#order-f-name');
  var order_l_name = $('#order-l-name');
  var order_email = $('#order-email');
  var order_address = $('#order-address');
  var order_city = $('#order-city');
  var order_country = $('#order-country');
  var order_state = $('#order-provinces');
  var order_zip = $('#order-zip');
  var order_phone = $('#order-phone');
  var order_province = $('#order-province');
  var order_courier = $('#order-courier');
  var order_kec = $('#order-kecamatan');
  var order_qty = $('#order-quantity');

  order_form.submit(function() { return false; });
  order_button.on('click', function(){
    // first name
    var firstnameval = order_f_name.val();
    var firstnamelen = firstnameval.length;

    if(firstnamelen < 1) {
      order_f_name.addClass('order-field-error');
    }
    else if(firstnamelen >= 1){
      order_f_name.removeClass('order-field-error');
    }

    // last name
    var lastnameval = order_l_name.val();
    var lastnamelen = lastnameval.length;

    if(lastnamelen < 1) {
      order_l_name.addClass('order-field-error');
    }
    else if(lastnamelen >= 1){
      order_l_name.removeClass('order-field-error');
    }

    // email
    var emailval   = order_email.val();
    var emailvalid = validateEmail(emailval);
    if(emailvalid === false) {
      order_email.addClass('order-field-error');
    }
    else if(emailvalid === true){
      order_email.removeClass('order-field-error');
    }

    // address
    var addressval = order_address.val();
    var addresslen = addressval.length;

    if(addresslen < 1) {
      order_address.addClass('order-field-error');
    }
    else if(addresslen >= 1){
      order_address.removeClass('order-field-error');
    }

    // city
    var cityval = order_city.val();
    var citylen = cityval.length;

    if(citylen < 1) {
      order_city.addClass('order-field-error');
    }
    else if(citylen >= 1){
      order_city.removeClass('order-field-error');
    }

    // country
    var countryval = $('#order-country option:selected').val();

    if(countryval == 0 ) {
      order_country.addClass('order-field-error');
    }
    else if(countryval != 0){
      order_country.removeClass('order-field-error');
    }

    // province
    var provinceval = $('#order-province').val();
    if(provinceval == 0 ) {
      order_province.addClass('order-field-error');
    }
    else if(provinceval != 0){
      order_province.removeClass('order-field-error');
    }

    // kec
    var kecval = $('#order-kecamatan').val();
    if(kecval == 0 ) {
      order_kec.addClass('order-field-error');
    }
    else if(kecval != 0){
      order_kec.removeClass('order-field-error');
    }

    // kurir
    var courierval = $('#order-courier').val();
    if(courierval == 0 ) {
      order_courier.addClass('order-field-error');
    }
    else if(courierval != 0){
      order_courier.removeClass('order-field-error');
    }

    // city
    var cityval = $('#order-city').val();
    if(cityval == 0 ) {
      order_city.addClass('order-field-error');
    }
    else if(provinceval != 0){
      order_city.removeClass('order-field-error');
    }

    var zipval = order_zip.val();
    var ziplen = zipval.length;
    if(ziplen < 1) {
      order_zip.addClass('order-field-error');
    }
    else if(ziplen >= 1){
      order_zip.removeClass('order-field-error');
    }

    // qty
    qtyval = parseInt(order_qty.val(),10);
    var qtyok = false;
    if(Number.isInteger(qtyval) && qtyval >= 1 ) {
      order_qty.removeClass('order-field-error');
      qtyok = true;
    }else{
      order_qty.addClass('order-field-error');
      qtyok = false;
    }

    // phone
    var phoneval = order_phone.val();
    var phonelen = phoneval.length;

    if(phonelen < 1) {
      order_phone.addClass('order-field-error');
    }
    else if(phonelen >= 1){
      order_phone.removeClass('order-field-error');
    }

    // payment
    var payment = $("#hd-payment").val().length;
    if(payment < 1) {
      $(".select_payment").addClass('order-field-error');
    }
    else if(payment >= 1){
      $(".select_payment").removeClass('order-field-error');
    }

    if(firstnamelen >= 1 && lastnamelen >= 1 && emailvalid === true && addresslen >= 1 &&  ziplen >= 1 && phonelen >= 1 && provinceval != 0 && cityval != 0 && courierval !=0 && kecval !=0 && qtyok) {
      order_button.replaceWith("<span class='order-send'>send...</span>");
      // function on view
      setOrder();
    }
  });
}

function setOrder(){
    $("#order-form").fadeOut('fast', function(){
        $(".select_payment").removeClass('order-field-error');
        $("#content-payment").fadeIn();
    });
}

function validateEmail(email) { 
  var reg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return reg.test(email);
}

initOrder();


// set payment
$(function(){
    payValue = "";
    $('body').on('click','.select_payment',function (e) {
        e.preventDefault();
        payValue = $(this).data('value');
        $(".select_payment.active").removeClass('active');
        $(this).addClass('active');
        $(".select_payment").removeClass('order-field-error');
        $("#hd-payment").val(payValue);
        $(this).addClass('active');
        $(".paysum").hide();
        var dsc = $(this).data('description');
        if (payValue == 'transfer') {
            $("#pay-sum-bank p.below").text(dsc);
            $("#pay-sum-bank").fadeIn();
            $("#hd-payment_detail").val($("#payment_transfer_option").val());
            confirmPay = "Transfer Bank : "+$("#payment_transfer_option option:selected").text();
        }else if(payValue == 'cod'){
            $("#pay-sum").find('p').text(dsc);
            $("#pay-sum").fadeIn();
            confirmPay = "Cash on Delivery";
        }else if(payValue == 'kredivo'){
            $("#pay-sum").find('p').text(dsc);
            $("#pay-sum").fadeIn();
            confirmPay = "Kredivo";
        }else{
            $("#pay-sum").find('p').text(dsc);
            $("#pay-sum").fadeIn();
            confirmPay = "Pay with Doku Wallet";
        }
    });

    $('body').on('click','#pay-coupon-have',function (e) {
        e.preventDefault();
        $(this).fadeOut(function(){
            $("#pay-coupon-form").fadeIn(function(){
                $("#coupon-input").focus();
            });
        });
    });
    $('body').on('click','#order-button-payment',function (e) {
        e.preventDefault();
        if (payValue == "") {
            $(".select_payment").addClass('order-field-error');
        }else{
            $("#content-payment").fadeOut(function(){
                setConfirmation();
                $("#content-confirmation").fadeIn();
            });
        }
    });
    $('body').on('click','#coupon-submit',function (e) {
        e.preventDefault();
        var csrf                = $("#global-form").find("input");
        var usercode            = $(this).parent().find('input');
        var opt                 = {};
        opt[csrf.attr('name')]  = csrf.val();
        opt['code']             = usercode.val();
        $('body').css('cursor', 'wait');
        $.post(BASE+"home/check_coupon",  opt,function(data){
            $('body').css('cursor', 'default');
            if (data.status == 'valid') {
                $("#hd-prd_coupon_code").val(usercode.val());
                $('#pay-coupon-form').fadeOut(function(){
                    $("#pay-coupon-valid").fadeIn().html("Selamat anda mendapatkan potongan <span class='sale-price' >Rp. "+ formatNumber(data.value)+'</span>');
                    coupon_final = data.value;
                })
            }else{
                $("#hd-prd_coupon_code").val("");
                $("#coupon-input").focus();
                coupon_final = 0;
                usercode.addClass('order-field-error');
            }
        },"json");
    });
})

function initHiddenForm(e){
    $("#hd-prd_id").val(e.data('id'));      
    $("#hd-prd_coupon_code").val('');
    $("#hd-prd_total").val('');
    $("#hd-payment").val('');
}

function setPrice(ths){
    real_prc = ths.data('price');
    real_prc_dsc = ths.data('price_dsc');
    
    var prc = formatNumber(real_prc);
    var dsc = formatNumber(real_prc_dsc);
    
    if (real_prc_dsc > 0 ) {
        return '<span class="sale">Rp '+prc+'</span><span class="sale-price">Rp '+dsc+'</span>';
    }else{
        return '<span>Rp.'+prc+'</span>';
    }
}


// set confirm
var order_status = 'incompleted';
$(function(){
    var ths;
    $('body').on('click','#order-button-final',function (e) {
        e.preventDefault();
        $("#order-step2").fadeOut(function(){
            ths = $(this);
            ths.before("<br><br><p id='confirm_loading'>Please wait . . .</p>");
            $.post(BASE+"home/place_order", $("#order-form").serialize(),function(data){
                order_status = 'completed';
                $("#confirm_loading").hide();
                if (data.status == "failed") {
                    ths.before("<br><br><p>Order gagal, silahkan coba lagi");
                }else{
                    ths.before("<br><br><p>Terimakasih sudah order!<br> Tunggu sebentar anda akan diarahkan ke halaman order");
                    var payment = data.payment;var url="";

                    if (payment.search("doku") >= 0) {
                        url = BASE+"home/order/"+data.link_unique;
                    }else{
                        url = BASE+"order_success/"+data.link_unique;
                    }

                    setTimeout(function () {
                        window.location.href = url;
                    }, 2000);
                }

            },'json');
        })
});
})

var price_final     = 0;
var grand_total     = 0;
var coupon_final    = 0;
var delivery_final  = 0;
var coupon_code_final = "";
var confirmKurir = "";
var confirmPay = "";

function setConfirmation(){
    price_final = ((real_prc_dsc > 0) ? real_prc_dsc : real_prc);
    price_final = price_final * qtyval;
    confirmKurir = $("#order-courier option:selected");
    delivery_final = confirmKurir.data('kurir-price');

    $("#confirm-ship").html("Rp. "+formatNumber(delivery_final));
    $("#confirm-ship-type").html(confirmKurir.data('kurir'));
    var qtytext = ((qtyval > 1)? " ("+qtyval+"X)" : "");
    $("#confirm-prd").text($("#pop-title").html()+qtytext);
    $("#confirm-price").text("Rp. "+formatNumber(price_final));

    if (coupon_final > 0) {
        $("#row-coupon").removeClass("hide");
        $("#confirm-coupon-code b").text($("#hd-prd_coupon_code").val());
        $("#confirm-coupon-code-value").text("Rp. "+formatNumber(coupon_final));
    }else{
        $("#row-coupon").addClass("hide");
    }

    grand_total = price_final + delivery_final - coupon_final;

    $("#confirm-grand-total").text("Rp. "+formatNumber(grand_total));

    $("#hd-prd_total").val(grand_total);

    setDeliveryDetail();
}

function setDeliveryDetail(){
    $("#deliver-name").html($("#order-f-name").val());
    $("#deliver-address").html($("#order-address").val());
    $("#deliver-prov").html($("#order-province option:selected").text()+" - "+$("#order-city option:selected").text()+"<br>Kec. "+$("#order-kecamatan option:selected").text()+", "+$("#order-zip").val());
    $("#deliver-email").html("Email : "+$("#order-email").val()+", HP : "+$("#order-phone").val());
    $("#deliver-shipping").html("Shipping Method : "+(confirmKurir.data("kurir")).toUpperCase());
    $("#deliver-payment").html("Payment : "+confirmPay);
}

function set_close_modal(){
    if (order_status == 'completed') {
        location.reload();
    }
}


// Slider
function initSlider() {

  var slider = $('.flexslider');
  var caption = $('.flex-caption');

  slider.flexslider({
    animation: 'fade',
    slideshowSpeed: 7000,   //Integer: Set the speed of the slideshow cycling, in milliseconds
    animationSpeed: 2000,   //Integer: Set the speed of animations, in milliseconds
    controlNav: false,
    prevText: '',
    nextText: '',

    start: function(){},
    before: function(){captionMoveOut();},
    after: function(){captionMoveIn();},
  });

  caption.hide();
  caption.fadeIn(700);
}

function captionMoveIn() {
  if ($(window).width() >= 768) {
    $('.flex-caption')
      .animate({left: '7.5rem'},0)
      .fadeIn(700)
  }
;};  

function captionMoveOut() {
  if ($(window).width() >= 768) {
    $('.flex-caption')
      .animate({left: '-100%'},900)
      .fadeOut('normal')
  }    
;};

$(function(){
    initSlider();
})

// slider
$(document).ready(function () { 
    var time = $('.count-down');
    if (time.length) {
      var endDate = new Date(time.data("end-date"));
      time.countdown({
        date: endDate,
        render: function (data) {
          $(this.el).html('<div class="cd-row"><div><h1>'
            + this.leadingZeros(data.hours, 2)
            + '</h1><p>hrs</p></div></div><div class="cd-row"><div><h1>'
            + this.leadingZeros(data.min, 2)
            + '</h1><p>min</p></div><div><h1>'
            + this.leadingZeros(data.sec, 2)
            + '</h1><p>sec</p></div></div>');
        }
      });
    }

 }
);